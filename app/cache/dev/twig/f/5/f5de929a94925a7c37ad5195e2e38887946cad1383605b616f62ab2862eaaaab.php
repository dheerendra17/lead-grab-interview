<?php

/* AppBundle:User/Email:registration.html.twig */
class __TwigTemplate_bcf8e8c8b401fd7ec80d21888e90f77dde96a7e733b7ef67f43e616a0d5297e0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FrontendBundle::layout.html.twig", "AppBundle:User/Email:registration.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FrontendBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f15209426fad073aa67d26b1d2e98dd3a10ff88333293ab5f9bbacb2b20ec037 = $this->env->getExtension("native_profiler");
        $__internal_f15209426fad073aa67d26b1d2e98dd3a10ff88333293ab5f9bbacb2b20ec037->enter($__internal_f15209426fad073aa67d26b1d2e98dd3a10ff88333293ab5f9bbacb2b20ec037_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AppBundle:User/Email:registration.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_f15209426fad073aa67d26b1d2e98dd3a10ff88333293ab5f9bbacb2b20ec037->leave($__internal_f15209426fad073aa67d26b1d2e98dd3a10ff88333293ab5f9bbacb2b20ec037_prof);

    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        $__internal_023929010fba6b6e9e75c9bc7562a9364e099ea0429fd1cea6caac751d563ad2 = $this->env->getExtension("native_profiler");
        $__internal_023929010fba6b6e9e75c9bc7562a9364e099ea0429fd1cea6caac751d563ad2->enter($__internal_023929010fba6b6e9e75c9bc7562a9364e099ea0429fd1cea6caac751d563ad2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        echo "\t
<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" id=\"backgroundTable\">
\t<tr>
\t\t
\tHi ";
        // line 6
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "username", array()), "html", null, true);
        echo ",</br>
\t\tPlease follow the link below to activate your account:</br>
\t\t<a href=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getUrl("user_authenticate_process", array("token" => $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "token", array()))), "html", null, true);
        echo "\">Comfirm your registration</a>

\t</tr>
</table>  
";
        
        $__internal_023929010fba6b6e9e75c9bc7562a9364e099ea0429fd1cea6caac751d563ad2->leave($__internal_023929010fba6b6e9e75c9bc7562a9364e099ea0429fd1cea6caac751d563ad2_prof);

    }

    public function getTemplateName()
    {
        return "AppBundle:User/Email:registration.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  50 => 8,  45 => 6,  34 => 2,  11 => 1,);
    }
}
/* {% extends 'FrontendBundle::layout.html.twig' %}*/
/* {% block content %}	*/
/* <table cellpadding="0" cellspacing="0" border="0" id="backgroundTable">*/
/* 	<tr>*/
/* 		*/
/* 	Hi {{user.username}},</br>*/
/* 		Please follow the link below to activate your account:</br>*/
/* 		<a href="{{ url('user_authenticate_process',{'token':user.token}) }}">Comfirm your registration</a>*/
/* */
/* 	</tr>*/
/* </table>  */
/* {% endblock %}*/
/* */
