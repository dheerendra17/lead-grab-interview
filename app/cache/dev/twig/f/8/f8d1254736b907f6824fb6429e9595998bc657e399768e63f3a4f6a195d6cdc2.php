<?php

/* AdminBundle:AdminUsers:adminProfile.html.twig */
class __TwigTemplate_14f7bb4b44f097ca189b0f864a098d0e9ceb3b68958642d35140d7bc5fe4fbfb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("AdminBundle::layout.html.twig", "AdminBundle:AdminUsers:adminProfile.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
            'breadcrumb' => array($this, 'block_breadcrumb'),
            'profile' => array($this, 'block_profile'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AdminBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d4b9063eccb56a068c53e7fef806c19bf2268a108715e218b56a2e99786838f5 = $this->env->getExtension("native_profiler");
        $__internal_d4b9063eccb56a068c53e7fef806c19bf2268a108715e218b56a2e99786838f5->enter($__internal_d4b9063eccb56a068c53e7fef806c19bf2268a108715e218b56a2e99786838f5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AdminBundle:AdminUsers:adminProfile.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_d4b9063eccb56a068c53e7fef806c19bf2268a108715e218b56a2e99786838f5->leave($__internal_d4b9063eccb56a068c53e7fef806c19bf2268a108715e218b56a2e99786838f5_prof);

    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        $__internal_2777d3d9ca01fc35dcd1667f16435971ef06ebbb1db96de1960b2d6b34f14bbb = $this->env->getExtension("native_profiler");
        $__internal_2777d3d9ca01fc35dcd1667f16435971ef06ebbb1db96de1960b2d6b34f14bbb->enter($__internal_2777d3d9ca01fc35dcd1667f16435971ef06ebbb1db96de1960b2d6b34f14bbb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 3
        $this->displayBlock('breadcrumb', $context, $blocks);
        // line 7
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashbag", array()), "get", array(0 => "notice"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 8
            echo "<div class=\"flash-notice\">
\t<h2 class=\"form-signin-heading btn btn-xs btn-success\">";
            // line 9
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</h2>
</div>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 12
        $this->displayBlock('profile', $context, $blocks);
        
        $__internal_2777d3d9ca01fc35dcd1667f16435971ef06ebbb1db96de1960b2d6b34f14bbb->leave($__internal_2777d3d9ca01fc35dcd1667f16435971ef06ebbb1db96de1960b2d6b34f14bbb_prof);

    }

    // line 3
    public function block_breadcrumb($context, array $blocks = array())
    {
        $__internal_dfde5cca56c033bdc51c8f74222e02189306ed836a63faaff2862eff6e628eb7 = $this->env->getExtension("native_profiler");
        $__internal_dfde5cca56c033bdc51c8f74222e02189306ed836a63faaff2862eff6e628eb7->enter($__internal_dfde5cca56c033bdc51c8f74222e02189306ed836a63faaff2862eff6e628eb7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "breadcrumb"));

        // line 4
        echo "<a class=\"btn btn-primary\" href=\"";
        echo $this->env->getExtension('routing')->getPath("admin_user_list");
        echo "\" role=\"button\">Back to the list <<</a>
<h2 class=\"sub-header\">User ";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans($this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "username", array())), "html", null, true);
        echo " Profile View</h2>
";
        
        $__internal_dfde5cca56c033bdc51c8f74222e02189306ed836a63faaff2862eff6e628eb7->leave($__internal_dfde5cca56c033bdc51c8f74222e02189306ed836a63faaff2862eff6e628eb7_prof);

    }

    // line 12
    public function block_profile($context, array $blocks = array())
    {
        $__internal_4dd276a36f0f13e4ddf8504890e1f692e4be00e1243d6462aed21327681ad423 = $this->env->getExtension("native_profiler");
        $__internal_4dd276a36f0f13e4ddf8504890e1f692e4be00e1243d6462aed21327681ad423->enter($__internal_4dd276a36f0f13e4ddf8504890e1f692e4be00e1243d6462aed21327681ad423_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "profile"));

        // line 13
        echo "\t<table class=\"table table-bordered\">
\t<tbody>
\t\t<tr>
\t\t\t<th>";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Id"), "html", null, true);
        echo "</th>
\t\t\t<td>";
        // line 17
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "id", array()), "html", null, true);
        echo "</td>
\t\t</tr>
\t\t<tr>
\t\t\t<th>";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("User"), "html", null, true);
        echo "</th>
\t\t\t<td>";
        // line 21
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "username", array()), "html", null, true);
        echo "</td>
\t\t</tr>
\t\t<tr>
\t\t\t<th>";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Password"), "html", null, true);
        echo "</th>
\t\t\t<td>";
        // line 25
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "plainpassword", array()), "html", null, true);
        echo "</td>
\t\t</tr>
\t</tbody>
\t</table>

\t<table class=\"table table-bordered\">
\t<tbody>
\t\t<tr>
\t\t\t<th>";
        // line 33
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("City Name"), "html", null, true);
        echo "</th>
\t\t\t<td>";
        // line 34
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "city", array()), "html", null, true);
        echo "</td>
\t\t</tr>
\t\t<tr>
\t\t\t<th>";
        // line 37
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Country"), "html", null, true);
        echo "</th>
\t\t\t<td>";
        // line 38
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "country", array()), "html", null, true);
        echo "</td>
\t\t</tr>
\t\t<tr>
\t\t\t<th>";
        // line 41
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("EMERGENCY PHONE 24 HOURS"), "html", null, true);
        echo "</th>
\t\t\t<td>";
        // line 42
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "phoneNumber", array()), "html", null, true);
        echo "</td>
\t\t</tr>
\t\t<tr>
\t\t\t<th>";
        // line 45
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Active"), "html", null, true);
        echo "</th>
\t\t\t<td>
\t\t\t\t";
        // line 47
        if (($this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "isActive", array()) == "1")) {
            // line 48
            echo "\t\t\t\t\tYES
\t\t\t\t";
        } else {
            // line 50
            echo "\t\t\t\t\tNO
\t\t\t\t";
        }
        // line 52
        echo "\t\t\t</td>
\t\t</tr>
\t\t<tr>
\t\t\t<th>";
        // line 55
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("ROLES"), "html", null, true);
        echo "</th>
\t\t\t<td>
\t\t\t\t";
        // line 57
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "roles", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["role"]) {
            // line 58
            echo "\t\t\t\t\t";
            echo twig_escape_filter($this->env, $context["role"], "html", null, true);
            echo "
\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['role'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 60
        echo "\t\t\t</td>
\t\t</tr>
\t</tbody>
\t</table>
";
        
        $__internal_4dd276a36f0f13e4ddf8504890e1f692e4be00e1243d6462aed21327681ad423->leave($__internal_4dd276a36f0f13e4ddf8504890e1f692e4be00e1243d6462aed21327681ad423_prof);

    }

    public function getTemplateName()
    {
        return "AdminBundle:AdminUsers:adminProfile.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  201 => 60,  192 => 58,  188 => 57,  183 => 55,  178 => 52,  174 => 50,  170 => 48,  168 => 47,  163 => 45,  157 => 42,  153 => 41,  147 => 38,  143 => 37,  137 => 34,  133 => 33,  122 => 25,  118 => 24,  112 => 21,  108 => 20,  102 => 17,  98 => 16,  93 => 13,  87 => 12,  78 => 5,  73 => 4,  67 => 3,  60 => 12,  51 => 9,  48 => 8,  44 => 7,  42 => 3,  36 => 2,  11 => 1,);
    }
}
/* {% extends 'AdminBundle::layout.html.twig' %}*/
/* {% block content %}*/
/* {% block breadcrumb %}*/
/* <a class="btn btn-primary" href="{{ path('admin_user_list') }}" role="button">Back to the list <<</a>*/
/* <h2 class="sub-header">User {{user.username|trans}} Profile View</h2>*/
/* {% endblock %}*/
/* {% for flashMessage in app.session.flashbag.get('notice') %}*/
/* <div class="flash-notice">*/
/* 	<h2 class="form-signin-heading btn btn-xs btn-success">{{ flashMessage }}</h2>*/
/* </div>*/
/* {% endfor %}*/
/* {% block profile %}*/
/* 	<table class="table table-bordered">*/
/* 	<tbody>*/
/* 		<tr>*/
/* 			<th>{{'Id'|trans}}</th>*/
/* 			<td>{{ user.id}}</td>*/
/* 		</tr>*/
/* 		<tr>*/
/* 			<th>{{'User'|trans}}</th>*/
/* 			<td>{{ user.username}}</td>*/
/* 		</tr>*/
/* 		<tr>*/
/* 			<th>{{'Password'|trans}}</th>*/
/* 			<td>{{ user.plainpassword}}</td>*/
/* 		</tr>*/
/* 	</tbody>*/
/* 	</table>*/
/* */
/* 	<table class="table table-bordered">*/
/* 	<tbody>*/
/* 		<tr>*/
/* 			<th>{{'City Name'|trans}}</th>*/
/* 			<td>{{ user.city}}</td>*/
/* 		</tr>*/
/* 		<tr>*/
/* 			<th>{{'Country'|trans}}</th>*/
/* 			<td>{{ user.country}}</td>*/
/* 		</tr>*/
/* 		<tr>*/
/* 			<th>{{'EMERGENCY PHONE 24 HOURS'|trans}}</th>*/
/* 			<td>{{ user.phoneNumber}}</td>*/
/* 		</tr>*/
/* 		<tr>*/
/* 			<th>{{'Active'|trans}}</th>*/
/* 			<td>*/
/* 				{% if user.isActive == '1' %}*/
/* 					YES*/
/* 				{% else %}*/
/* 					NO*/
/* 				{% endif %}*/
/* 			</td>*/
/* 		</tr>*/
/* 		<tr>*/
/* 			<th>{{'ROLES'|trans}}</th>*/
/* 			<td>*/
/* 				{% for role in user.roles %}*/
/* 					{{ role }}*/
/* 				{% endfor %}*/
/* 			</td>*/
/* 		</tr>*/
/* 	</tbody>*/
/* 	</table>*/
/* {% endblock %}*/
/* {% endblock %}*/
/* */
/* */
