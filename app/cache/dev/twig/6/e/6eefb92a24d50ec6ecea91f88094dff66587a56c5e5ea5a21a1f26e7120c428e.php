<?php

/* ::base.html.twig */
class __TwigTemplate_c7972c239df77eee5251c2056cf79286e08083323581adca43e7452ff72884a1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'javascriptss' => array($this, 'block_javascriptss'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7c093168b014b9f9ac894837f901515f784f7db79112cd2816145344ab70cec5 = $this->env->getExtension("native_profiler");
        $__internal_7c093168b014b9f9ac894837f901515f784f7db79112cd2816145344ab70cec5->enter($__internal_7c093168b014b9f9ac894837f901515f784f7db79112cd2816145344ab70cec5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "::base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
\t\t";
        // line 4
        $this->displayBlock('head', $context, $blocks);
        // line 6
        echo "\t\t<meta charset=\"UTF-8\" />
\t\t<title>";
        // line 7
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
\t\t<link rel=\"icon\" href=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/images/favicon.ico"), "html", null, true);
        echo "\">
\t\t";
        // line 9
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 17
        echo "\t\t";
        $this->displayBlock('javascriptss', $context, $blocks);
        // line 20
        echo "    </head>
    <body>
        ";
        // line 22
        $this->displayBlock('body', $context, $blocks);
        // line 24
        echo "        ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 29
        echo "    </body>
</html>
";
        
        $__internal_7c093168b014b9f9ac894837f901515f784f7db79112cd2816145344ab70cec5->leave($__internal_7c093168b014b9f9ac894837f901515f784f7db79112cd2816145344ab70cec5_prof);

    }

    // line 4
    public function block_head($context, array $blocks = array())
    {
        $__internal_88136865e89e9383a48aeb3fd2fa877d594fe1e4d44cf87b3f6398ebe9187a93 = $this->env->getExtension("native_profiler");
        $__internal_88136865e89e9383a48aeb3fd2fa877d594fe1e4d44cf87b3f6398ebe9187a93->enter($__internal_88136865e89e9383a48aeb3fd2fa877d594fe1e4d44cf87b3f6398ebe9187a93_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 5
        echo "\t\t";
        
        $__internal_88136865e89e9383a48aeb3fd2fa877d594fe1e4d44cf87b3f6398ebe9187a93->leave($__internal_88136865e89e9383a48aeb3fd2fa877d594fe1e4d44cf87b3f6398ebe9187a93_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_6c0558d9fd65254b86594d744fba6b0235f8d4b8a3d5b73e4789beb797ca62e4 = $this->env->getExtension("native_profiler");
        $__internal_6c0558d9fd65254b86594d744fba6b0235f8d4b8a3d5b73e4789beb797ca62e4->enter($__internal_6c0558d9fd65254b86594d744fba6b0235f8d4b8a3d5b73e4789beb797ca62e4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Welcome LeadGrab !";
        
        $__internal_6c0558d9fd65254b86594d744fba6b0235f8d4b8a3d5b73e4789beb797ca62e4->leave($__internal_6c0558d9fd65254b86594d744fba6b0235f8d4b8a3d5b73e4789beb797ca62e4_prof);

    }

    // line 9
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_69e83f97376f2647ceb1cb396beb44e6d6ff6db11a757ecc42b6e0385c0a8b7b = $this->env->getExtension("native_profiler");
        $__internal_69e83f97376f2647ceb1cb396beb44e6d6ff6db11a757ecc42b6e0385c0a8b7b->enter($__internal_69e83f97376f2647ceb1cb396beb44e6d6ff6db11a757ecc42b6e0385c0a8b7b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 10
        echo "\t\t\t<!-- Bootstrap core CSS -->
\t\t\t<link href=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/css/bootstrap.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
\t\t\t<!-- Custom styles for this template -->
\t\t\t<link href=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/css/navbar.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
\t\t\t<link href=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/css/dashboard.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
\t\t\t<link href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/css/signin.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
\t\t";
        
        $__internal_69e83f97376f2647ceb1cb396beb44e6d6ff6db11a757ecc42b6e0385c0a8b7b->leave($__internal_69e83f97376f2647ceb1cb396beb44e6d6ff6db11a757ecc42b6e0385c0a8b7b_prof);

    }

    // line 17
    public function block_javascriptss($context, array $blocks = array())
    {
        $__internal_0fa8dfdd832a22d2f4efda1a4e9d8c8fecc33f548b1cb276e7716615bb68de04 = $this->env->getExtension("native_profiler");
        $__internal_0fa8dfdd832a22d2f4efda1a4e9d8c8fecc33f548b1cb276e7716615bb68de04->enter($__internal_0fa8dfdd832a22d2f4efda1a4e9d8c8fecc33f548b1cb276e7716615bb68de04_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascriptss"));

        // line 18
        echo "\t\t\t<script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/js/ie-emulation-modes-warning.js"), "html", null, true);
        echo "\"></script>
\t\t";
        
        $__internal_0fa8dfdd832a22d2f4efda1a4e9d8c8fecc33f548b1cb276e7716615bb68de04->leave($__internal_0fa8dfdd832a22d2f4efda1a4e9d8c8fecc33f548b1cb276e7716615bb68de04_prof);

    }

    // line 22
    public function block_body($context, array $blocks = array())
    {
        $__internal_6bdfcdb38a795317cad9add7db7def0c1581b85c6550d4e7af62b5aa7deb799f = $this->env->getExtension("native_profiler");
        $__internal_6bdfcdb38a795317cad9add7db7def0c1581b85c6550d4e7af62b5aa7deb799f->enter($__internal_6bdfcdb38a795317cad9add7db7def0c1581b85c6550d4e7af62b5aa7deb799f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 23
        echo "        ";
        
        $__internal_6bdfcdb38a795317cad9add7db7def0c1581b85c6550d4e7af62b5aa7deb799f->leave($__internal_6bdfcdb38a795317cad9add7db7def0c1581b85c6550d4e7af62b5aa7deb799f_prof);

    }

    // line 24
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_623259dd38eedfd6aa536be1d1c8aa374dfebf2ae07d88eba4fb9b2f002bf199 = $this->env->getExtension("native_profiler");
        $__internal_623259dd38eedfd6aa536be1d1c8aa374dfebf2ae07d88eba4fb9b2f002bf199->enter($__internal_623259dd38eedfd6aa536be1d1c8aa374dfebf2ae07d88eba4fb9b2f002bf199_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 25
        echo "\t\t\t<script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/js/jquery.min.js"), "html", null, true);
        echo "\"></script>
\t\t\t<script src=\"";
        // line 26
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
\t\t\t<script src=\"";
        // line 27
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/js/ie10-viewport-bug-workaround.js"), "html", null, true);
        echo "\"></script>
\t\t";
        
        $__internal_623259dd38eedfd6aa536be1d1c8aa374dfebf2ae07d88eba4fb9b2f002bf199->leave($__internal_623259dd38eedfd6aa536be1d1c8aa374dfebf2ae07d88eba4fb9b2f002bf199_prof);

    }

    public function getTemplateName()
    {
        return "::base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  169 => 27,  165 => 26,  160 => 25,  154 => 24,  147 => 23,  141 => 22,  131 => 18,  125 => 17,  116 => 15,  112 => 14,  108 => 13,  103 => 11,  100 => 10,  94 => 9,  82 => 7,  75 => 5,  69 => 4,  60 => 29,  57 => 24,  55 => 22,  51 => 20,  48 => 17,  46 => 9,  42 => 8,  38 => 7,  35 => 6,  33 => 4,  28 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html>*/
/*     <head>*/
/* 		{% block head %}*/
/* 		{% endblock %}*/
/* 		<meta charset="UTF-8" />*/
/* 		<title>{% block title %}Welcome LeadGrab !{% endblock %}</title>*/
/* 		<link rel="icon" href="{{ asset('bundles/frontend/images/favicon.ico') }}">*/
/* 		{% block stylesheets %}*/
/* 			<!-- Bootstrap core CSS -->*/
/* 			<link href="{{ asset('bundles/frontend/css/bootstrap.min.css') }}" rel="stylesheet">*/
/* 			<!-- Custom styles for this template -->*/
/* 			<link href="{{ asset('bundles/frontend/css/navbar.css') }}" rel="stylesheet">*/
/* 			<link href="{{ asset('bundles/frontend/css/dashboard.css') }}" rel="stylesheet">*/
/* 			<link href="{{ asset('bundles/frontend/css/signin.css') }}" rel="stylesheet">*/
/* 		{% endblock %}*/
/* 		{% block javascriptss %}*/
/* 			<script src="{{ asset('bundles/frontend/js/ie-emulation-modes-warning.js') }}"></script>*/
/* 		{% endblock %}*/
/*     </head>*/
/*     <body>*/
/*         {% block body %}*/
/*         {% endblock %}*/
/*         {% block javascripts %}*/
/* 			<script src="{{ asset('bundles/frontend/js/jquery.min.js') }}"></script>*/
/* 			<script src="{{ asset('bundles/frontend/js/bootstrap.min.js') }}"></script>*/
/* 			<script src="{{ asset('bundles/frontend/js/ie10-viewport-bug-workaround.js') }}"></script>*/
/* 		{% endblock %}*/
/*     </body>*/
/* </html>*/
/* */
