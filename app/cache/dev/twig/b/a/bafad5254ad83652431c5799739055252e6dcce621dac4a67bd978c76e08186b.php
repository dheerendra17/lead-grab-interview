<?php

/* AdminBundle:AdminUsers:login.html.twig */
class __TwigTemplate_6aa8740a13d9d7ce3fec16fb14bbbb89432358d290b4be78ba78a7aedc4d9e26 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "AdminBundle:AdminUsers:login.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1c8120b7e06d6532c1473d3f704cfcec3805a5ac5e14af0ffc6d93ab9f7e3931 = $this->env->getExtension("native_profiler");
        $__internal_1c8120b7e06d6532c1473d3f704cfcec3805a5ac5e14af0ffc6d93ab9f7e3931->enter($__internal_1c8120b7e06d6532c1473d3f704cfcec3805a5ac5e14af0ffc6d93ab9f7e3931_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AdminBundle:AdminUsers:login.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_1c8120b7e06d6532c1473d3f704cfcec3805a5ac5e14af0ffc6d93ab9f7e3931->leave($__internal_1c8120b7e06d6532c1473d3f704cfcec3805a5ac5e14af0ffc6d93ab9f7e3931_prof);

    }

    // line 2
    public function block_body($context, array $blocks = array())
    {
        $__internal_71ce7500a6eb6ce7eccd70d28c54a2a03d421ba3e0954c326a355834b992f5e2 = $this->env->getExtension("native_profiler");
        $__internal_71ce7500a6eb6ce7eccd70d28c54a2a03d421ba3e0954c326a355834b992f5e2->enter($__internal_71ce7500a6eb6ce7eccd70d28c54a2a03d421ba3e0954c326a355834b992f5e2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 3
        echo "<div class=\"container\">
\t";
        // line 4
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashbag", array()), "get", array(0 => "notice"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 5
            echo "\t<div class=\"flash-notice\">
\t\t<h2 class=\"btn btn-xs btn-danger\">";
            // line 6
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</h2>
\t</div>
\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 9
        echo "\t<form action=\"";
        echo $this->env->getExtension('routing')->getPath("admin_login_check");
        echo "\" method=\"post\" class=\"form-signin\">
\t\t<h2 class=\"form-signin-heading\">Admin Login !</h2>
\t\t<label class=\"sr-only\" for=\"username\">Username*</label>
\t\t\t<input type=\"text\" id=\"inputUsername\" class=\"form-control\" name=\"_username\" placeholder=\"Username\" required=\"\" autofocus=\"\">
\t\t<label class=\"sr-only\" for=\"password\">Password*</label>
\t\t\t<input type=\"password\" id=\"inputPassword\" class=\"form-control\" name=\"_password\" placeholder=\"Password\" required=\"\">
\t\t<button class=\"btn btn-lg btn-primary btn-block\" type=\"submit\">Sign in</button>\t</form>
\t</form>
</div>
";
        
        $__internal_71ce7500a6eb6ce7eccd70d28c54a2a03d421ba3e0954c326a355834b992f5e2->leave($__internal_71ce7500a6eb6ce7eccd70d28c54a2a03d421ba3e0954c326a355834b992f5e2_prof);

    }

    public function getTemplateName()
    {
        return "AdminBundle:AdminUsers:login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  59 => 9,  50 => 6,  47 => 5,  43 => 4,  40 => 3,  34 => 2,  11 => 1,);
    }
}
/* {% extends '::base.html.twig' %}*/
/* {% block body %}*/
/* <div class="container">*/
/* 	{% for flashMessage in app.session.flashbag.get('notice') %}*/
/* 	<div class="flash-notice">*/
/* 		<h2 class="btn btn-xs btn-danger">{{ flashMessage }}</h2>*/
/* 	</div>*/
/* 	{% endfor %}*/
/* 	<form action="{{ path('admin_login_check') }}" method="post" class="form-signin">*/
/* 		<h2 class="form-signin-heading">Admin Login !</h2>*/
/* 		<label class="sr-only" for="username">Username*</label>*/
/* 			<input type="text" id="inputUsername" class="form-control" name="_username" placeholder="Username" required="" autofocus="">*/
/* 		<label class="sr-only" for="password">Password*</label>*/
/* 			<input type="password" id="inputPassword" class="form-control" name="_password" placeholder="Password" required="">*/
/* 		<button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>	</form>*/
/* 	</form>*/
/* </div>*/
/* {% endblock %}*/
/* */
