<?php

/* AppBundle:User:login.html.twig */
class __TwigTemplate_9ad401ecd67ab3fdf37832d47f4a4d1096adfdaf4e3b0803ec0a0eb3e2883b91 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FrontendBundle::layout.html.twig", "AppBundle:User:login.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FrontendBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_796944308496591d8678f854dc0b6b85e6d3ae94dc18ca831eb1733a4ec4a13a = $this->env->getExtension("native_profiler");
        $__internal_796944308496591d8678f854dc0b6b85e6d3ae94dc18ca831eb1733a4ec4a13a->enter($__internal_796944308496591d8678f854dc0b6b85e6d3ae94dc18ca831eb1733a4ec4a13a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AppBundle:User:login.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_796944308496591d8678f854dc0b6b85e6d3ae94dc18ca831eb1733a4ec4a13a->leave($__internal_796944308496591d8678f854dc0b6b85e6d3ae94dc18ca831eb1733a4ec4a13a_prof);

    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        $__internal_d829255356124dc84540d634ea2c6260ead4f51cb885661badd8173484db1306 = $this->env->getExtension("native_profiler");
        $__internal_d829255356124dc84540d634ea2c6260ead4f51cb885661badd8173484db1306->enter($__internal_d829255356124dc84540d634ea2c6260ead4f51cb885661badd8173484db1306_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 3
        echo "<div class=\"container\">
\t\t";
        // line 4
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashbag", array()), "get", array(0 => "success"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 5
            echo "\t\t<div class=\"flash-notice\">
\t\t\t<h2 class=\"btn btn-xs btn-success\">";
            // line 6
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</h2>
\t\t</div>
\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 9
        echo "\t\t";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashbag", array()), "get", array(0 => "error"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 10
            echo "\t\t<div class=\"flash-notice\">
\t\t\t<h2 class=\"btn btn-xs btn-danger\">";
            // line 11
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</h2>
\t\t</div>
\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 14
        echo "\t\t";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashbag", array()), "get", array(0 => "login_error"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 15
            echo "\t\t<div class=\"flash-notice\">
\t\t\t<h2 class=\"btn btn-xs btn-danger\">";
            // line 16
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</h2>
\t\t</div>
\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 19
        echo "\t\t<form class=\"form-signin\" action=\"";
        echo $this->env->getExtension('routing')->getPath("user_login_check");
        echo "\" method=\"post\" >
\t\t\t<h2 class=\"form-signin-heading\">Please sign in</h2>
\t\t\t<label for=\"inputUsername\" class=\"sr-only\">Username</label>
\t\t\t\t<input type=\"text\" id=\"inputUsername\" class=\"form-control\" name=\"_username\" placeholder=\"Username\" required=\"\" autofocus=\"\">
\t\t\t<label for=\"inputPassword\" class=\"sr-only\">Password</label>
\t\t\t\t<input type=\"password\" id=\"inputPassword\" class=\"form-control\" name=\"_password\" placeholder=\"Password\" required=\"\">
\t\t\t<button class=\"btn btn-lg btn-primary btn-block\" type=\"submit\">Sign in</button>
\t\t</form>
\t\t<div style=\"float:right; font-size: 100%; position: relative; top:-10px\"><a href=\"";
        // line 27
        echo $this->env->getExtension('routing')->getPath("user_forgot_password");
        echo "\">Forgot password?</a></div></br>
</div>
";
        
        $__internal_d829255356124dc84540d634ea2c6260ead4f51cb885661badd8173484db1306->leave($__internal_d829255356124dc84540d634ea2c6260ead4f51cb885661badd8173484db1306_prof);

    }

    public function getTemplateName()
    {
        return "AppBundle:User:login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  105 => 27,  93 => 19,  84 => 16,  81 => 15,  76 => 14,  67 => 11,  64 => 10,  59 => 9,  50 => 6,  47 => 5,  43 => 4,  40 => 3,  34 => 2,  11 => 1,);
    }
}
/* {% extends 'FrontendBundle::layout.html.twig' %}*/
/* {% block content %}*/
/* <div class="container">*/
/* 		{% for flashMessage in app.session.flashbag.get('success') %}*/
/* 		<div class="flash-notice">*/
/* 			<h2 class="btn btn-xs btn-success">{{ flashMessage }}</h2>*/
/* 		</div>*/
/* 		{% endfor %}*/
/* 		{% for flashMessage in app.session.flashbag.get('error') %}*/
/* 		<div class="flash-notice">*/
/* 			<h2 class="btn btn-xs btn-danger">{{ flashMessage }}</h2>*/
/* 		</div>*/
/* 		{% endfor %}*/
/* 		{% for flashMessage in app.session.flashbag.get('login_error') %}*/
/* 		<div class="flash-notice">*/
/* 			<h2 class="btn btn-xs btn-danger">{{ flashMessage }}</h2>*/
/* 		</div>*/
/* 		{% endfor %}*/
/* 		<form class="form-signin" action="{{ path('user_login_check') }}" method="post" >*/
/* 			<h2 class="form-signin-heading">Please sign in</h2>*/
/* 			<label for="inputUsername" class="sr-only">Username</label>*/
/* 				<input type="text" id="inputUsername" class="form-control" name="_username" placeholder="Username" required="" autofocus="">*/
/* 			<label for="inputPassword" class="sr-only">Password</label>*/
/* 				<input type="password" id="inputPassword" class="form-control" name="_password" placeholder="Password" required="">*/
/* 			<button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>*/
/* 		</form>*/
/* 		<div style="float:right; font-size: 100%; position: relative; top:-10px"><a href="{{path('user_forgot_password')}}">Forgot password?</a></div></br>*/
/* </div>*/
/* {% endblock %}*/
/* */
