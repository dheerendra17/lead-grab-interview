<?php

/* AppBundle:User:register.html.twig */
class __TwigTemplate_bc479ea11220644afb1d14c9f501f054d8fa7de8dbde21ad18fdc11d16a3bec4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FrontendBundle::layout.html.twig", "AppBundle:User:register.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FrontendBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_987721005013d00c648c95a5ffd568bdddc6b31b17c6048135f22782f5fbf61d = $this->env->getExtension("native_profiler");
        $__internal_987721005013d00c648c95a5ffd568bdddc6b31b17c6048135f22782f5fbf61d->enter($__internal_987721005013d00c648c95a5ffd568bdddc6b31b17c6048135f22782f5fbf61d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AppBundle:User:register.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_987721005013d00c648c95a5ffd568bdddc6b31b17c6048135f22782f5fbf61d->leave($__internal_987721005013d00c648c95a5ffd568bdddc6b31b17c6048135f22782f5fbf61d_prof);

    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        $__internal_066866f7bb4203c0d5b6b6894370aa191225a7e531f1a20ac46a28420bc7890a = $this->env->getExtension("native_profiler");
        $__internal_066866f7bb4203c0d5b6b6894370aa191225a7e531f1a20ac46a28420bc7890a->enter($__internal_066866f7bb4203c0d5b6b6894370aa191225a7e531f1a20ac46a28420bc7890a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 3
        echo "<div class=\"container\">
    \t\t<form action=\"";
        // line 4
        echo $this->env->getExtension('routing')->getPath("user_register");
        echo "\" method=\"POST\" class=\"form-signin\" autocomplete=\"off\" novalidate >
\t\t\t<h2 class=\"form-signin-heading\">Create Account</h2>
\t\t\t\t<div class=\"control-group\">
\t\t\t\t\t<label class=\"control-label\" for=\"inputEmail\">Username*</label>
\t\t\t\t\t<div class=\"controls\">
\t\t\t\t\t\t";
        // line 9
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "username", array()), 'widget', array("attr" => array("class" => "form-control", "placeholder" => "Username")));
        echo "
\t\t\t\t\t\t";
        // line 10
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "username", array()), 'errors');
        echo "
\t     \t\t\t</div>
\t     \t\t</div>
\t\t\t\t<div class=\"control-group\">
\t\t\t\t\t<label class=\"control-label\" for=\"inputEmail\">Email*</label>
\t\t\t\t\t<div class=\"controls\">
\t\t\t\t\t\t";
        // line 16
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "email", array()), 'widget', array("attr" => array("class" => "form-control", "placeholder" => "Email")));
        echo "
\t\t\t\t\t\t";
        // line 17
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "email", array()), 'errors');
        echo "
\t     \t\t\t</div>
\t     \t\t</div>
\t\t\t\t<div class=\"control-group\">
\t\t\t\t\t\t<label class=\"control-label\" for=\"inputPassword\">Password*</label>
\t\t\t\t\t<div class=\"controls\">
\t\t\t\t\t\t";
        // line 23
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "plainpassword", array()), "first", array()), 'widget', array("attr" => array("class" => "form-control", "placeholder" => "Password", "required" => false)));
        echo "
\t\t\t\t\t\t";
        // line 24
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "plainpassword", array()), "first", array()), 'errors');
        echo "
\t\t\t\t\t\t<span style=\"color: red; font-size: 11px; display: block;\">Maximum Length of password should be 6</span>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"control-group\">
\t\t\t\t\t\t<label class=\"control-label\" for=\"inputRePassword\">Retype Password*</label>
\t\t\t\t\t<div class=\"controls\">
\t\t\t\t\t\t";
        // line 31
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "plainpassword", array()), "second", array()), 'widget', array("attr" => array("class" => "form-control", "placeholder" => "Confirm Password", "required" => false)));
        echo "
\t\t\t\t\t\t";
        // line 32
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "plainpassword", array()), "second", array()), 'errors');
        echo "
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t";
        // line 35
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'rest');
        echo "
\t\t\t\t<div class=\"control-group marginTop10\">
\t\t\t\t\t<div class=\"controls\">
\t\t\t\t\t\t<input type=\"submit\" class=\"btn btn-lg btn-primary btn-block\" value=\"Register\" />
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</form>
\t\t\t</div>
";
        
        $__internal_066866f7bb4203c0d5b6b6894370aa191225a7e531f1a20ac46a28420bc7890a->leave($__internal_066866f7bb4203c0d5b6b6894370aa191225a7e531f1a20ac46a28420bc7890a_prof);

    }

    public function getTemplateName()
    {
        return "AppBundle:User:register.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  101 => 35,  95 => 32,  91 => 31,  81 => 24,  77 => 23,  68 => 17,  64 => 16,  55 => 10,  51 => 9,  43 => 4,  40 => 3,  34 => 2,  11 => 1,);
    }
}
/* {% extends 'FrontendBundle::layout.html.twig' %}*/
/* {% block content %}*/
/* <div class="container">*/
/*     		<form action="{{ path('user_register') }}" method="POST" class="form-signin" autocomplete="off" novalidate >*/
/* 			<h2 class="form-signin-heading">Create Account</h2>*/
/* 				<div class="control-group">*/
/* 					<label class="control-label" for="inputEmail">Username*</label>*/
/* 					<div class="controls">*/
/* 						{{ form_widget(form.username, { 'attr':{class:'form-control','placeholder':'Username'} }) }}*/
/* 						{{ form_errors(form.username)}}*/
/* 	     			</div>*/
/* 	     		</div>*/
/* 				<div class="control-group">*/
/* 					<label class="control-label" for="inputEmail">Email*</label>*/
/* 					<div class="controls">*/
/* 						{{ form_widget(form.email, { 'attr':{class:'form-control','placeholder':'Email'} }) }}*/
/* 						{{ form_errors(form.email)}}*/
/* 	     			</div>*/
/* 	     		</div>*/
/* 				<div class="control-group">*/
/* 						<label class="control-label" for="inputPassword">Password*</label>*/
/* 					<div class="controls">*/
/* 						{{ form_widget(form.plainpassword.first, { 'attr':{class:'form-control','placeholder':'Password','required': false} }) }}*/
/* 						{{form_errors(form.plainpassword.first)}}*/
/* 						<span style="color: red; font-size: 11px; display: block;">Maximum Length of password should be 6</span>*/
/* 					</div>*/
/* 				</div>*/
/* 				<div class="control-group">*/
/* 						<label class="control-label" for="inputRePassword">Retype Password*</label>*/
/* 					<div class="controls">*/
/* 						{{ form_widget(form.plainpassword.second, { 'attr':{class:'form-control','placeholder':'Confirm Password','required': false} }) }}*/
/* 						{{form_errors(form.plainpassword.second)}}*/
/* 					</div>*/
/* 				</div>*/
/* 				{{ form_rest(form) }}*/
/* 				<div class="control-group marginTop10">*/
/* 					<div class="controls">*/
/* 						<input type="submit" class="btn btn-lg btn-primary btn-block" value="Register" />*/
/* 					</div>*/
/* 				</div>*/
/* 			</form>*/
/* 			</div>*/
/* {% endblock %}*/
/* */
