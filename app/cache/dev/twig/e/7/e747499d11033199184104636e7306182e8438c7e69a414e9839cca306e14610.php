<?php

/* AdminBundle::layout.html.twig */
class __TwigTemplate_16dc24c8c305970111c6ce7e4312e4043aafb7f71045ef45c7f794c00455cc90 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "AdminBundle::layout.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'header' => array($this, 'block_header'),
            'content' => array($this, 'block_content'),
            'footer' => array($this, 'block_footer'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_591072de108595ef65ccec8d7a32bf97b0e3c0be4db1a244c62212968a196599 = $this->env->getExtension("native_profiler");
        $__internal_591072de108595ef65ccec8d7a32bf97b0e3c0be4db1a244c62212968a196599->enter($__internal_591072de108595ef65ccec8d7a32bf97b0e3c0be4db1a244c62212968a196599_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AdminBundle::layout.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_591072de108595ef65ccec8d7a32bf97b0e3c0be4db1a244c62212968a196599->leave($__internal_591072de108595ef65ccec8d7a32bf97b0e3c0be4db1a244c62212968a196599_prof);

    }

    // line 2
    public function block_body($context, array $blocks = array())
    {
        $__internal_f799507bb0de47b3f1d4ec54112dec451511fcd7b8509475e6e7bd344e744b72 = $this->env->getExtension("native_profiler");
        $__internal_f799507bb0de47b3f1d4ec54112dec451511fcd7b8509475e6e7bd344e744b72->enter($__internal_f799507bb0de47b3f1d4ec54112dec451511fcd7b8509475e6e7bd344e744b72_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 3
        echo "\t";
        $this->displayBlock('header', $context, $blocks);
        // line 33
        echo "    <div class=\"container-fluid\">
      <div class=\"row\">
        <div class=\"col-sm-3 col-md-2 sidebar\">
          <ul class=\"nav nav-sidebar\">
            <li class=\"#\"><a href=\"";
        // line 37
        echo $this->env->getExtension('routing')->getPath("_admin");
        echo "\">Overview <span class=\"sr-only\">(current)</span></a></li>
            <li class=\"#\"><a href=\"";
        // line 38
        echo $this->env->getExtension('routing')->getPath("admin_customers");
        echo "\">Customers</a></li>
            <li class=\"#\"><a href=\"";
        // line 39
        echo $this->env->getExtension('routing')->getPath("admin_user_list");
        echo "\">Users</a></li>
            <li class=\"#\"><a href=\"";
        // line 40
        echo $this->env->getExtension('routing')->getPath("admin_roles");
        echo "\">Roles</a></li>
          </ul>
        </div>
        <div class=\"col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main\">
          <h1 class=\"page-header\">Dashboard</h1>
\t\t";
        // line 45
        $this->displayBlock('content', $context, $blocks);
        // line 47
        echo "        </div>
      </div>
    </div>
\t";
        // line 50
        $this->displayBlock('footer', $context, $blocks);
        
        $__internal_f799507bb0de47b3f1d4ec54112dec451511fcd7b8509475e6e7bd344e744b72->leave($__internal_f799507bb0de47b3f1d4ec54112dec451511fcd7b8509475e6e7bd344e744b72_prof);

    }

    // line 3
    public function block_header($context, array $blocks = array())
    {
        $__internal_a3f0195a2b85ca8670c494587d4235e85275c26a675704c2347a1d48e9e8acaa = $this->env->getExtension("native_profiler");
        $__internal_a3f0195a2b85ca8670c494587d4235e85275c26a675704c2347a1d48e9e8acaa->enter($__internal_a3f0195a2b85ca8670c494587d4235e85275c26a675704c2347a1d48e9e8acaa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header"));

        // line 4
        echo "    <nav class=\"navbar navbar-inverse navbar-fixed-top\">
      <div class=\"container-fluid\">
        <div class=\"navbar-header\">
          <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#navbar\" aria-expanded=\"false\" aria-controls=\"navbar\">
            <span class=\"sr-only\">Toggle navigation</span>
            <span class=\"icon-bar\"></span>
            <span class=\"icon-bar\"></span>
            <span class=\"icon-bar\"></span>
          </button>
          <a class=\"navbar-brand\" href=\"";
        // line 13
        echo $this->env->getExtension('routing')->getPath("_admin");
        echo "\">LeadGrab Admin</a>
        </div>
        <div id=\"navbar\" class=\"navbar-collapse collapse\">
\t\t<ul class=\"nav navbar-nav navbar-right\">
\t\t<li><a href=\"";
        // line 17
        echo $this->env->getExtension('routing')->getPath("_admin");
        echo "\">Dashboard</a></li>
\t\t</ul>
\t\t";
        // line 19
        if ($this->env->getExtension('security')->isGranted("IS_AUTHENTICATED_REMEMBERED")) {
            // line 20
            echo "\t\t<ul class=\"nav navbar-nav navbar-right\">
\t\t\t<li><a href=\"";
            // line 21
            echo $this->env->getExtension('routing')->getPath("admin_user_profile_show");
            echo "\">Profile <span class=\"sr-only\">(current)</span></a></li>
\t\t\t<li><a href=\"";
            // line 22
            echo $this->env->getExtension('routing')->getPath("admin_logout");
            echo "\">Logout <span class=\"sr-only\">(current)</span></a></li>
\t\t</ul>
\t\t";
        } else {
            // line 25
            echo "\t\t<ul class=\"nav navbar-nav navbar-right\">
\t\t\t<li><a href=\"";
            // line 26
            echo $this->env->getExtension('routing')->getPath("admin_login");
            echo "\">Login <span class=\"sr-only\">(current)</span></a></li>
\t\t</ul>
\t\t";
        }
        // line 29
        echo "        </div>
      </div>
    </nav>
\t";
        
        $__internal_a3f0195a2b85ca8670c494587d4235e85275c26a675704c2347a1d48e9e8acaa->leave($__internal_a3f0195a2b85ca8670c494587d4235e85275c26a675704c2347a1d48e9e8acaa_prof);

    }

    // line 45
    public function block_content($context, array $blocks = array())
    {
        $__internal_308e95df4d331896d49d6ee1730c8e00c63257541e98a56c689410f9a6fca478 = $this->env->getExtension("native_profiler");
        $__internal_308e95df4d331896d49d6ee1730c8e00c63257541e98a56c689410f9a6fca478->enter($__internal_308e95df4d331896d49d6ee1730c8e00c63257541e98a56c689410f9a6fca478_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 46
        echo "\t\t";
        
        $__internal_308e95df4d331896d49d6ee1730c8e00c63257541e98a56c689410f9a6fca478->leave($__internal_308e95df4d331896d49d6ee1730c8e00c63257541e98a56c689410f9a6fca478_prof);

    }

    // line 50
    public function block_footer($context, array $blocks = array())
    {
        $__internal_b7d530d68c30faa40e68c26cd9d05351e39a0e08a5c1d3fe44d7d476077a522d = $this->env->getExtension("native_profiler");
        $__internal_b7d530d68c30faa40e68c26cd9d05351e39a0e08a5c1d3fe44d7d476077a522d->enter($__internal_b7d530d68c30faa40e68c26cd9d05351e39a0e08a5c1d3fe44d7d476077a522d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "footer"));

        // line 51
        echo "\t";
        
        $__internal_b7d530d68c30faa40e68c26cd9d05351e39a0e08a5c1d3fe44d7d476077a522d->leave($__internal_b7d530d68c30faa40e68c26cd9d05351e39a0e08a5c1d3fe44d7d476077a522d_prof);

    }

    public function getTemplateName()
    {
        return "AdminBundle::layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  168 => 51,  162 => 50,  155 => 46,  149 => 45,  139 => 29,  133 => 26,  130 => 25,  124 => 22,  120 => 21,  117 => 20,  115 => 19,  110 => 17,  103 => 13,  92 => 4,  86 => 3,  79 => 50,  74 => 47,  72 => 45,  64 => 40,  60 => 39,  56 => 38,  52 => 37,  46 => 33,  43 => 3,  37 => 2,  11 => 1,);
    }
}
/* {% extends '::base.html.twig' %}*/
/* {% block body %}*/
/* 	{% block header %}*/
/*     <nav class="navbar navbar-inverse navbar-fixed-top">*/
/*       <div class="container-fluid">*/
/*         <div class="navbar-header">*/
/*           <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">*/
/*             <span class="sr-only">Toggle navigation</span>*/
/*             <span class="icon-bar"></span>*/
/*             <span class="icon-bar"></span>*/
/*             <span class="icon-bar"></span>*/
/*           </button>*/
/*           <a class="navbar-brand" href="{{path('_admin')}}">LeadGrab Admin</a>*/
/*         </div>*/
/*         <div id="navbar" class="navbar-collapse collapse">*/
/* 		<ul class="nav navbar-nav navbar-right">*/
/* 		<li><a href="{{path('_admin')}}">Dashboard</a></li>*/
/* 		</ul>*/
/* 		{% if is_granted("IS_AUTHENTICATED_REMEMBERED") %}*/
/* 		<ul class="nav navbar-nav navbar-right">*/
/* 			<li><a href="{{ path('admin_user_profile_show') }}">Profile <span class="sr-only">(current)</span></a></li>*/
/* 			<li><a href="{{ path('admin_logout') }}">Logout <span class="sr-only">(current)</span></a></li>*/
/* 		</ul>*/
/* 		{% else %}*/
/* 		<ul class="nav navbar-nav navbar-right">*/
/* 			<li><a href="{{ path('admin_login') }}">Login <span class="sr-only">(current)</span></a></li>*/
/* 		</ul>*/
/* 		{% endif %}*/
/*         </div>*/
/*       </div>*/
/*     </nav>*/
/* 	{% endblock %}*/
/*     <div class="container-fluid">*/
/*       <div class="row">*/
/*         <div class="col-sm-3 col-md-2 sidebar">*/
/*           <ul class="nav nav-sidebar">*/
/*             <li class="#"><a href="{{path('_admin')}}">Overview <span class="sr-only">(current)</span></a></li>*/
/*             <li class="#"><a href="{{path('admin_customers')}}">Customers</a></li>*/
/*             <li class="#"><a href="{{path('admin_user_list')}}">Users</a></li>*/
/*             <li class="#"><a href="{{path('admin_roles')}}">Roles</a></li>*/
/*           </ul>*/
/*         </div>*/
/*         <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">*/
/*           <h1 class="page-header">Dashboard</h1>*/
/* 		{% block content %}*/
/* 		{% endblock %}*/
/*         </div>*/
/*       </div>*/
/*     </div>*/
/* 	{% block footer %}*/
/* 	{% endblock %}*/
/* {% endblock %}*/
/* */
