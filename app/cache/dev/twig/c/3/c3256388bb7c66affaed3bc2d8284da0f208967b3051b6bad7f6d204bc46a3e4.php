<?php

/* TwigBundle:Exception:traces.txt.twig */
class __TwigTemplate_ff7a1a73e5b67eee70aae656dd5d4670977b18d4bdaa3bdb80cb93bb8d71172e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6ef1cf3b80e87d6a9dbccb0f7293a8ed8099c183d7499c6df7bfbf0d2eaaa3dc = $this->env->getExtension("native_profiler");
        $__internal_6ef1cf3b80e87d6a9dbccb0f7293a8ed8099c183d7499c6df7bfbf0d2eaaa3dc->enter($__internal_6ef1cf3b80e87d6a9dbccb0f7293a8ed8099c183d7499c6df7bfbf0d2eaaa3dc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:traces.txt.twig"));

        // line 1
        if (twig_length_filter($this->env, $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "trace", array()))) {
            // line 2
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "trace", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["trace"]) {
                // line 3
                $this->loadTemplate("TwigBundle:Exception:trace.txt.twig", "TwigBundle:Exception:traces.txt.twig", 3)->display(array("trace" => $context["trace"]));
                // line 4
                echo "
";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['trace'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
        }
        
        $__internal_6ef1cf3b80e87d6a9dbccb0f7293a8ed8099c183d7499c6df7bfbf0d2eaaa3dc->leave($__internal_6ef1cf3b80e87d6a9dbccb0f7293a8ed8099c183d7499c6df7bfbf0d2eaaa3dc_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:traces.txt.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  30 => 4,  28 => 3,  24 => 2,  22 => 1,);
    }
}
/* {% if exception.trace|length %}*/
/* {% for trace in exception.trace %}*/
/* {% include 'TwigBundle:Exception:trace.txt.twig' with { 'trace': trace } only %}*/
/* */
/* {% endfor %}*/
/* {% endif %}*/
/* */
