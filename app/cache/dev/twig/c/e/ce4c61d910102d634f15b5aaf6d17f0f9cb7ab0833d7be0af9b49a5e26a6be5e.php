<?php

/* AppBundle:User:profile.html.twig */
class __TwigTemplate_224573e2ecc46eaae3998dd79eae8803e2d64a957cd128c21486ed0a910857f8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FrontendBundle::layout.html.twig", "AppBundle:User:profile.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
            'breadcrumb' => array($this, 'block_breadcrumb'),
            'profile' => array($this, 'block_profile'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FrontendBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6bc3eb2d59a6fdd9056f2c69dc37270b2eb8a466c10834e8f0ea418cf67e99a0 = $this->env->getExtension("native_profiler");
        $__internal_6bc3eb2d59a6fdd9056f2c69dc37270b2eb8a466c10834e8f0ea418cf67e99a0->enter($__internal_6bc3eb2d59a6fdd9056f2c69dc37270b2eb8a466c10834e8f0ea418cf67e99a0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AppBundle:User:profile.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_6bc3eb2d59a6fdd9056f2c69dc37270b2eb8a466c10834e8f0ea418cf67e99a0->leave($__internal_6bc3eb2d59a6fdd9056f2c69dc37270b2eb8a466c10834e8f0ea418cf67e99a0_prof);

    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        $__internal_2299aa16b2ae160cbe8770fad6f4f22bd49204bc0660f1f0a813f18b76591576 = $this->env->getExtension("native_profiler");
        $__internal_2299aa16b2ae160cbe8770fad6f4f22bd49204bc0660f1f0a813f18b76591576->enter($__internal_2299aa16b2ae160cbe8770fad6f4f22bd49204bc0660f1f0a813f18b76591576_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 3
        $this->displayBlock('breadcrumb', $context, $blocks);
        // line 6
        $this->displayBlock('profile', $context, $blocks);
        // line 62
        echo "<p>
  <a class=\"btn btn-lg btn-primary\" href=\"";
        // line 63
        echo $this->env->getExtension('routing')->getPath("user_complete_profile");
        echo "\" role=\"button\">Update Your Profile &raquo;</a>
</p>
";
        
        $__internal_2299aa16b2ae160cbe8770fad6f4f22bd49204bc0660f1f0a813f18b76591576->leave($__internal_2299aa16b2ae160cbe8770fad6f4f22bd49204bc0660f1f0a813f18b76591576_prof);

    }

    // line 3
    public function block_breadcrumb($context, array $blocks = array())
    {
        $__internal_d9128e5f91d5c8216ff660acc297a4ec615a44ff243c4411d7d0574d239c1047 = $this->env->getExtension("native_profiler");
        $__internal_d9128e5f91d5c8216ff660acc297a4ec615a44ff243c4411d7d0574d239c1047->enter($__internal_d9128e5f91d5c8216ff660acc297a4ec615a44ff243c4411d7d0574d239c1047_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "breadcrumb"));

        // line 4
        echo "    <li class=\"last-breadcrumb\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("View Profile"), "html", null, true);
        echo "</li>
";
        
        $__internal_d9128e5f91d5c8216ff660acc297a4ec615a44ff243c4411d7d0574d239c1047->leave($__internal_d9128e5f91d5c8216ff660acc297a4ec615a44ff243c4411d7d0574d239c1047_prof);

    }

    // line 6
    public function block_profile($context, array $blocks = array())
    {
        $__internal_8feca0499efe6c121e477fe1f2907c25e8989bd99dd511d3994fd3c1e7a9e8e8 = $this->env->getExtension("native_profiler");
        $__internal_8feca0499efe6c121e477fe1f2907c25e8989bd99dd511d3994fd3c1e7a9e8e8->enter($__internal_8feca0499efe6c121e477fe1f2907c25e8989bd99dd511d3994fd3c1e7a9e8e8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "profile"));

        // line 7
        echo "\t";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashbag", array()), "get", array(0 => "success"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 8
            echo "\t<div class=\"flash-notice\">
\t\t<h2 class=\"btn btn-xs btn-success\">";
            // line 9
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</h2>
\t</div>
\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 12
        echo "\t";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashbag", array()), "get", array(0 => "error"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 13
            echo "\t<div class=\"flash-notice\">
\t\t<h2 class=\"btn btn-xs btn-danger\">";
            // line 14
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</h2>
\t</div>
\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 17
        echo "    <div class=\"span4\">(";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "username", array()), "html", null, true);
        echo ") ";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Profile"), "html", null, true);
        echo "</div>
    <table class=\"table table-bordered\">
        <tbody>
            <tr>
                <th>";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("User"), "html", null, true);
        echo " :";
        echo "</th>
                <td>";
        // line 22
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "username", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Password"), "html", null, true);
        echo " :";
        echo "</th>
                <td>";
        // line 26
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "plainpassword", array()), "html", null, true);
        echo "</td>
            </tr>
        </tbody>
    </table>

    <table class=\"table table-bordered\">
        <tbody>
            <tr>
                <th>";
        // line 34
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Gender"), "html", null, true);
        echo " :";
        echo "</th>
                <td>
\t\t\t\t\t";
        // line 36
        if (($this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "gender", array()) == "1")) {
            // line 37
            echo "                     Male
                    ";
        } elseif (($this->getAttribute(        // line 38
(isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "gender", array()) == "2")) {
            // line 39
            echo "                     Female
                    ";
        }
        // line 41
        echo "                </td>
            </tr>
            <tr>
                <th>";
        // line 44
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("City"), "html", null, true);
        echo " :";
        echo "</th>
                <td>
                     ";
        // line 46
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "city", array()), "html", null, true);
        echo "
                </td>
            </tr>
            <tr>
                <th>";
        // line 50
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Country"), "html", null, true);
        echo " :";
        echo "</th>
                <td>
                     ";
        // line 52
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "country", array()), "html", null, true);
        echo "
                </td>
            </tr>
            <tr style=\"color:#F44242\">
                <th>";
        // line 56
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("EMERGENCY PHONE 24 HOURS"), "html", null, true);
        echo " :";
        echo "</th>
                <td>";
        // line 57
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "phoneNumber", array()), "html", null, true);
        echo "</td>
            </tr>
        </tbody>
    </table>
";
        
        $__internal_8feca0499efe6c121e477fe1f2907c25e8989bd99dd511d3994fd3c1e7a9e8e8->leave($__internal_8feca0499efe6c121e477fe1f2907c25e8989bd99dd511d3994fd3c1e7a9e8e8_prof);

    }

    public function getTemplateName()
    {
        return "AppBundle:User:profile.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  205 => 57,  200 => 56,  193 => 52,  187 => 50,  180 => 46,  174 => 44,  169 => 41,  165 => 39,  163 => 38,  160 => 37,  158 => 36,  152 => 34,  141 => 26,  136 => 25,  130 => 22,  125 => 21,  115 => 17,  106 => 14,  103 => 13,  98 => 12,  89 => 9,  86 => 8,  81 => 7,  75 => 6,  65 => 4,  59 => 3,  49 => 63,  46 => 62,  44 => 6,  42 => 3,  36 => 2,  11 => 1,);
    }
}
/* {% extends 'FrontendBundle::layout.html.twig' %}*/
/* {% block content %}*/
/* {% block breadcrumb %}*/
/*     <li class="last-breadcrumb">{{'View Profile'|trans}}</li>*/
/* {% endblock %}*/
/* {% block profile %}*/
/* 	{% for flashMessage in app.session.flashbag.get('success') %}*/
/* 	<div class="flash-notice">*/
/* 		<h2 class="btn btn-xs btn-success">{{ flashMessage }}</h2>*/
/* 	</div>*/
/* 	{% endfor %}*/
/* 	{% for flashMessage in app.session.flashbag.get('error') %}*/
/* 	<div class="flash-notice">*/
/* 		<h2 class="btn btn-xs btn-danger">{{ flashMessage }}</h2>*/
/* 	</div>*/
/* 	{% endfor %}*/
/*     <div class="span4">({{app.user.username}}) {{'Profile'|trans}}</div>*/
/*     <table class="table table-bordered">*/
/*         <tbody>*/
/*             <tr>*/
/*                 <th>{{'User'|trans}}{{' :'}}</th>*/
/*                 <td>{{ user.username}}</td>*/
/*             </tr>*/
/*             <tr>*/
/*                 <th>{{'Password'|trans}}{{' :'}}</th>*/
/*                 <td>{{ user.plainpassword}}</td>*/
/*             </tr>*/
/*         </tbody>*/
/*     </table>*/
/* */
/*     <table class="table table-bordered">*/
/*         <tbody>*/
/*             <tr>*/
/*                 <th>{{'Gender'|trans}}{{' :'}}</th>*/
/*                 <td>*/
/* 					{% if user.gender == '1' %}*/
/*                      Male*/
/*                     {% elseif user.gender == '2' %}*/
/*                      Female*/
/*                     {% endif %}*/
/*                 </td>*/
/*             </tr>*/
/*             <tr>*/
/*                 <th>{{'City'|trans}}{{' :'}}</th>*/
/*                 <td>*/
/*                      {{ user.city}}*/
/*                 </td>*/
/*             </tr>*/
/*             <tr>*/
/*                 <th>{{'Country'|trans}}{{' :'}}</th>*/
/*                 <td>*/
/*                      {{ user.country}}*/
/*                 </td>*/
/*             </tr>*/
/*             <tr style="color:#F44242">*/
/*                 <th>{{'EMERGENCY PHONE 24 HOURS'|trans}}{{' :'}}</th>*/
/*                 <td>{{ user.phoneNumber}}</td>*/
/*             </tr>*/
/*         </tbody>*/
/*     </table>*/
/* {% endblock %}*/
/* <p>*/
/*   <a class="btn btn-lg btn-primary" href="{{path('user_complete_profile')}}" role="button">Update Your Profile &raquo;</a>*/
/* </p>*/
/* {% endblock %}*/
/* */
