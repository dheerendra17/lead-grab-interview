<?php

/* AppBundle:User:forgotPassword.html.twig */
class __TwigTemplate_0d9f9e9ccab7982b63bd638fee22dcebfe7f8934c513b7f0f74ad30f50240551 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FrontendBundle::layout.html.twig", "AppBundle:User:forgotPassword.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FrontendBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_fa15beb44b5593f856a799a209e9dfeaa6dc718c546ad9e1c69814a058e7cfe6 = $this->env->getExtension("native_profiler");
        $__internal_fa15beb44b5593f856a799a209e9dfeaa6dc718c546ad9e1c69814a058e7cfe6->enter($__internal_fa15beb44b5593f856a799a209e9dfeaa6dc718c546ad9e1c69814a058e7cfe6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AppBundle:User:forgotPassword.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_fa15beb44b5593f856a799a209e9dfeaa6dc718c546ad9e1c69814a058e7cfe6->leave($__internal_fa15beb44b5593f856a799a209e9dfeaa6dc718c546ad9e1c69814a058e7cfe6_prof);

    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        $__internal_4a582b10b2826d08c6b0b8f167348ed876e4a518e930ec2c0fa112b6bb7b66fb = $this->env->getExtension("native_profiler");
        $__internal_4a582b10b2826d08c6b0b8f167348ed876e4a518e930ec2c0fa112b6bb7b66fb->enter($__internal_4a582b10b2826d08c6b0b8f167348ed876e4a518e930ec2c0fa112b6bb7b66fb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 3
        echo "<div class=\"container\">
\t\t";
        // line 4
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashbag", array()), "get", array(0 => "success"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 5
            echo "\t\t<div class=\"flash-notice\">
\t\t\t<h2 class=\"btn btn-xs btn-success\">";
            // line 6
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</h2>
\t\t</div>
\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 9
        echo "\t\t";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashbag", array()), "get", array(0 => "error"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 10
            echo "\t\t<div class=\"flash-notice\">
\t\t\t<h2 class=\"btn btn-xs btn-danger\">";
            // line 11
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</h2>
\t\t</div>
\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 14
        echo "    <form action=\"";
        echo $this->env->getExtension('routing')->getPath("user_forgot_password");
        echo "\" method=\"post\" class=\"form-signin\">
\t\t<h2 class=\"form-signin-heading\">Forgot Password !</h2>
\t\t<div class=\"control-group\">
\t\t\t<div class=\"controls\">
\t\t\t\t";
        // line 18
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "email", array()), 'widget', array("attr" => array("class" => "form-control", "placeholder" => "Email")));
        echo "
\t\t\t\t";
        // line 19
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "email", array()), 'errors');
        echo "
\t\t\t</div>
\t\t</div>
        ";
        // line 22
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'rest');
        echo "
        <div class=\"control-group\">
            <div class=\"controls\">
                <input type=\"submit\" value=\"";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Submit"), "html", null, true);
        echo "\" class=\"btn btn-lg btn-primary btn-block\" />
            </div>
        </div>
    </form>
</div>
";
        
        $__internal_4a582b10b2826d08c6b0b8f167348ed876e4a518e930ec2c0fa112b6bb7b66fb->leave($__internal_4a582b10b2826d08c6b0b8f167348ed876e4a518e930ec2c0fa112b6bb7b66fb_prof);

    }

    public function getTemplateName()
    {
        return "AppBundle:User:forgotPassword.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  100 => 25,  94 => 22,  88 => 19,  84 => 18,  76 => 14,  67 => 11,  64 => 10,  59 => 9,  50 => 6,  47 => 5,  43 => 4,  40 => 3,  34 => 2,  11 => 1,);
    }
}
/* {% extends 'FrontendBundle::layout.html.twig' %}*/
/* {% block content %}*/
/* <div class="container">*/
/* 		{% for flashMessage in app.session.flashbag.get('success') %}*/
/* 		<div class="flash-notice">*/
/* 			<h2 class="btn btn-xs btn-success">{{ flashMessage }}</h2>*/
/* 		</div>*/
/* 		{% endfor %}*/
/* 		{% for flashMessage in app.session.flashbag.get('error') %}*/
/* 		<div class="flash-notice">*/
/* 			<h2 class="btn btn-xs btn-danger">{{ flashMessage }}</h2>*/
/* 		</div>*/
/* 		{% endfor %}*/
/*     <form action="{{ path('user_forgot_password') }}" method="post" class="form-signin">*/
/* 		<h2 class="form-signin-heading">Forgot Password !</h2>*/
/* 		<div class="control-group">*/
/* 			<div class="controls">*/
/* 				{{ form_widget(form.email, { 'attr':{class:'form-control','placeholder':'Email'} }) }}*/
/* 				{{ form_errors(form.email) }}*/
/* 			</div>*/
/* 		</div>*/
/*         {{ form_rest(form) }}*/
/*         <div class="control-group">*/
/*             <div class="controls">*/
/*                 <input type="submit" value="{{'Submit'|trans}}" class="btn btn-lg btn-primary btn-block" />*/
/*             </div>*/
/*         </div>*/
/*     </form>*/
/* </div>*/
/* {% endblock %}*/
/* */
