<?php

/* AdminBundle:Admin:index.html.twig */
class __TwigTemplate_ce7e7ac033a1fd652b38edf4a1dfc5087b624509e1627a21e7eef155a5c7972a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("AdminBundle::layout.html.twig", "AdminBundle:Admin:index.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AdminBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b2a54b788ca9c87eb9430af5e1b5ace343dd9c46de403856df51b7c0ea2a6ab4 = $this->env->getExtension("native_profiler");
        $__internal_b2a54b788ca9c87eb9430af5e1b5ace343dd9c46de403856df51b7c0ea2a6ab4->enter($__internal_b2a54b788ca9c87eb9430af5e1b5ace343dd9c46de403856df51b7c0ea2a6ab4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AdminBundle:Admin:index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_b2a54b788ca9c87eb9430af5e1b5ace343dd9c46de403856df51b7c0ea2a6ab4->leave($__internal_b2a54b788ca9c87eb9430af5e1b5ace343dd9c46de403856df51b7c0ea2a6ab4_prof);

    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        $__internal_b665f759f9e6a0e65227d9fed47b73e6486719742d5191e7cd5d432be43898ae = $this->env->getExtension("native_profiler");
        $__internal_b665f759f9e6a0e65227d9fed47b73e6486719742d5191e7cd5d432be43898ae->enter($__internal_b665f759f9e6a0e65227d9fed47b73e6486719742d5191e7cd5d432be43898ae_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 3
        echo "<h2 class=\"sub-header\">
\t<a class=\"btn btn-lg btn-info\" href=\"";
        // line 4
        echo $this->env->getExtension('routing')->getPath("admin_customers");
        echo "\" role=\"button\">Customers</a>
\t<a class=\"btn btn-lg btn-success\" href=\"";
        // line 5
        echo $this->env->getExtension('routing')->getPath("admin_user_list");
        echo "\" role=\"button\">User</a>
\t<a class=\"btn btn-lg btn-warning\" href=\"";
        // line 6
        echo $this->env->getExtension('routing')->getPath("admin_roles");
        echo "\" role=\"button\">Roles</a>
</h2>
";
        
        $__internal_b665f759f9e6a0e65227d9fed47b73e6486719742d5191e7cd5d432be43898ae->leave($__internal_b665f759f9e6a0e65227d9fed47b73e6486719742d5191e7cd5d432be43898ae_prof);

    }

    public function getTemplateName()
    {
        return "AdminBundle:Admin:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  51 => 6,  47 => 5,  43 => 4,  40 => 3,  34 => 2,  11 => 1,);
    }
}
/* {% extends 'AdminBundle::layout.html.twig' %}*/
/* {% block content %}*/
/* <h2 class="sub-header">*/
/* 	<a class="btn btn-lg btn-info" href="{{ path('admin_customers') }}" role="button">Customers</a>*/
/* 	<a class="btn btn-lg btn-success" href="{{ path('admin_user_list') }}" role="button">User</a>*/
/* 	<a class="btn btn-lg btn-warning" href="{{ path('admin_roles') }}" role="button">Roles</a>*/
/* </h2>*/
/* {% endblock %}*/
/* */
