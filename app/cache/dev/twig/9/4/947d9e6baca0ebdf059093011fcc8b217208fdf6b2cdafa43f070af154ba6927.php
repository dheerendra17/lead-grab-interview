<?php

/* FrontendBundle:Frontend:index.html.twig */
class __TwigTemplate_d01d18e8cd677f28f60b6d1918171d9f4f08102d390b85e157ae26d067159abc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FrontendBundle::layout.html.twig", "FrontendBundle:Frontend:index.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FrontendBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b8041637a95d02726eb17910e72040531b89d65d657b59b3fbb1678621eff095 = $this->env->getExtension("native_profiler");
        $__internal_b8041637a95d02726eb17910e72040531b89d65d657b59b3fbb1678621eff095->enter($__internal_b8041637a95d02726eb17910e72040531b89d65d657b59b3fbb1678621eff095_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FrontendBundle:Frontend:index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_b8041637a95d02726eb17910e72040531b89d65d657b59b3fbb1678621eff095->leave($__internal_b8041637a95d02726eb17910e72040531b89d65d657b59b3fbb1678621eff095_prof);

    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        $__internal_b0d2e5e0f31c1894f492cbee4b6ae3969f80ba56da9446526b7149cc8845ae5a = $this->env->getExtension("native_profiler");
        $__internal_b0d2e5e0f31c1894f492cbee4b6ae3969f80ba56da9446526b7149cc8845ae5a->enter($__internal_b0d2e5e0f31c1894f492cbee4b6ae3969f80ba56da9446526b7149cc8845ae5a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 3
        echo "      <div class=\"jumbotron\">
        <h1>All Page's List</h1>
        <p>Customer's  Page</p>
        <p>
          <a class=\"btn btn-lg btn-primary\" href=\"";
        // line 7
        echo $this->env->getExtension('routing')->getPath("customers");
        echo "\" role=\"button\">View &raquo;</a>
        </p>
        </br>
      </div>
";
        
        $__internal_b0d2e5e0f31c1894f492cbee4b6ae3969f80ba56da9446526b7149cc8845ae5a->leave($__internal_b0d2e5e0f31c1894f492cbee4b6ae3969f80ba56da9446526b7149cc8845ae5a_prof);

    }

    public function getTemplateName()
    {
        return "FrontendBundle:Frontend:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  46 => 7,  40 => 3,  34 => 2,  11 => 1,);
    }
}
/* {% extends 'FrontendBundle::layout.html.twig' %}*/
/* {% block content %}*/
/*       <div class="jumbotron">*/
/*         <h1>All Page's List</h1>*/
/*         <p>Customer's  Page</p>*/
/*         <p>*/
/*           <a class="btn btn-lg btn-primary" href="{{path('customers')}}" role="button">View &raquo;</a>*/
/*         </p>*/
/*         </br>*/
/*       </div>*/
/* {% endblock %}*/
/* */
