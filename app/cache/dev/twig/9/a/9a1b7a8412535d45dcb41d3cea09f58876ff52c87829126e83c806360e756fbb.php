<?php

/* FrontendBundle:Customers:index.html.twig */
class __TwigTemplate_fe970dbf34f5c52b900fb85321324c6c2f86b9fd1ad160448c98607892efe623 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FrontendBundle::layout.html.twig", "FrontendBundle:Customers:index.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FrontendBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8ac89b19a99680f237fa9db01005d9296cdc53ec7c3f7212858795eeb55a1095 = $this->env->getExtension("native_profiler");
        $__internal_8ac89b19a99680f237fa9db01005d9296cdc53ec7c3f7212858795eeb55a1095->enter($__internal_8ac89b19a99680f237fa9db01005d9296cdc53ec7c3f7212858795eeb55a1095_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FrontendBundle:Customers:index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_8ac89b19a99680f237fa9db01005d9296cdc53ec7c3f7212858795eeb55a1095->leave($__internal_8ac89b19a99680f237fa9db01005d9296cdc53ec7c3f7212858795eeb55a1095_prof);

    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        $__internal_ce6f1d433c63cde5ba1439995743871ea26eba43184b1328f420041183668e59 = $this->env->getExtension("native_profiler");
        $__internal_ce6f1d433c63cde5ba1439995743871ea26eba43184b1328f420041183668e59->enter($__internal_ce6f1d433c63cde5ba1439995743871ea26eba43184b1328f420041183668e59_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 3
        echo "<div class=\"jumbotron\">
        <h1>Customer's List</h1>
        ";
        // line 5
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["entities"]) ? $context["entities"] : $this->getContext($context, "entities")));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["entity"]) {
            // line 6
            echo "\t\t\t<p>";
            echo twig_escape_filter($this->env, $this->getAttribute($context["loop"], "index", array()), "html", null, true);
            echo ". ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "name", array()), "html", null, true);
            echo "</p>
\t\t\t<p>
\t\t\t  <a class=\"btn btn-lg btn-primary\" href=\"";
            // line 8
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("customers_show", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
            echo "\" role=\"button\">Show »</a>
\t\t\t</p>
\t\t\t</br>
\t\t";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entity'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 12
        echo "</div>
";
        
        $__internal_ce6f1d433c63cde5ba1439995743871ea26eba43184b1328f420041183668e59->leave($__internal_ce6f1d433c63cde5ba1439995743871ea26eba43184b1328f420041183668e59_prof);

    }

    public function getTemplateName()
    {
        return "FrontendBundle:Customers:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  87 => 12,  69 => 8,  61 => 6,  44 => 5,  40 => 3,  34 => 2,  11 => 1,);
    }
}
/* {% extends 'FrontendBundle::layout.html.twig' %}*/
/* {% block content %}*/
/* <div class="jumbotron">*/
/*         <h1>Customer's List</h1>*/
/*         {% for entity in entities %}*/
/* 			<p>{{loop.index}}. {{ entity.name }}</p>*/
/* 			<p>*/
/* 			  <a class="btn btn-lg btn-primary" href="{{ path('customers_show', { 'id': entity.id }) }}" role="button">Show »</a>*/
/* 			</p>*/
/* 			</br>*/
/* 		{% endfor %}*/
/* </div>*/
/* {% endblock %}*/
/* */
