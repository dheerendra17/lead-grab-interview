<?php

/* FrontendBundle::layout.html.twig */
class __TwigTemplate_4dd1bbcedc078cd3ca6ffedff67bc7690e0b8580b2ebdddebaea1193ed852a15 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "FrontendBundle::layout.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'header' => array($this, 'block_header'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_30c3f0e99c93e1310d0cfdb498860c8e0310272adc264d5389a3c78df1bac555 = $this->env->getExtension("native_profiler");
        $__internal_30c3f0e99c93e1310d0cfdb498860c8e0310272adc264d5389a3c78df1bac555->enter($__internal_30c3f0e99c93e1310d0cfdb498860c8e0310272adc264d5389a3c78df1bac555_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FrontendBundle::layout.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_30c3f0e99c93e1310d0cfdb498860c8e0310272adc264d5389a3c78df1bac555->leave($__internal_30c3f0e99c93e1310d0cfdb498860c8e0310272adc264d5389a3c78df1bac555_prof);

    }

    // line 2
    public function block_body($context, array $blocks = array())
    {
        $__internal_0bd524ea9886f90949965327eee0b89fb4c0bd2a2a656aab33e08c81d1fa3463 = $this->env->getExtension("native_profiler");
        $__internal_0bd524ea9886f90949965327eee0b89fb4c0bd2a2a656aab33e08c81d1fa3463->enter($__internal_0bd524ea9886f90949965327eee0b89fb4c0bd2a2a656aab33e08c81d1fa3463_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 3
        echo "<div class=\"container\">
\t";
        // line 4
        $this->displayBlock('header', $context, $blocks);
        // line 33
        echo "\t";
        $this->displayBlock('content', $context, $blocks);
        // line 35
        echo "</div>
";
        
        $__internal_0bd524ea9886f90949965327eee0b89fb4c0bd2a2a656aab33e08c81d1fa3463->leave($__internal_0bd524ea9886f90949965327eee0b89fb4c0bd2a2a656aab33e08c81d1fa3463_prof);

    }

    // line 4
    public function block_header($context, array $blocks = array())
    {
        $__internal_9ab209e0001262560eb35460e4f138b1188b705fc096ad7c2dcb4aa9de17fc3e = $this->env->getExtension("native_profiler");
        $__internal_9ab209e0001262560eb35460e4f138b1188b705fc096ad7c2dcb4aa9de17fc3e->enter($__internal_9ab209e0001262560eb35460e4f138b1188b705fc096ad7c2dcb4aa9de17fc3e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header"));

        // line 5
        echo "      <nav class=\"navbar navbar-default\">
        <div class=\"container-fluid\">
          <div class=\"navbar-header\">
            <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#navbar\" aria-expanded=\"false\" aria-controls=\"navbar\">
              <span class=\"sr-only\">Toggle navigation</span>
              <span class=\"icon-bar\"></span>
              <span class=\"icon-bar\"></span>
              <span class=\"icon-bar\"></span>
            </button>
\t\t\t\t<a class=\"navbar-brand\" href=\"";
        // line 14
        echo $this->env->getExtension('routing')->getPath("_home");
        echo "\">LeadGrab </a>
          </div>
          <div id=\"navbar\" class=\"navbar-collapse collapse\">
            <ul class=\"nav navbar-nav\">
              <li class=\"active\"><a href=\"";
        // line 18
        echo $this->env->getExtension('routing')->getPath("_home");
        echo "\">Home</a></li>
            </ul>
            <ul class=\"nav navbar-nav navbar-right\">
\t\t\t\t";
        // line 21
        if ($this->env->getExtension('security')->isGranted("IS_AUTHENTICATED_REMEMBERED")) {
            // line 22
            echo "\t\t\t\t\t<li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("user_profile_show");
            echo "\">Profile <span class=\"sr-only\">(current)</span></a></li>
\t\t\t\t\t<li><a href=\"";
            // line 23
            echo $this->env->getExtension('routing')->getPath("user_logout");
            echo "\">Logout <span class=\"sr-only\">(current)</span></a></li>
\t\t\t\t";
        } else {
            // line 25
            echo "\t\t\t\t\t<li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("user_login");
            echo "\">Login <span class=\"sr-only\">(current)</span></a></li>
\t\t\t\t\t<li><a href=\"";
            // line 26
            echo $this->env->getExtension('routing')->getPath("user_register");
            echo "\">Register <span class=\"sr-only\">(current)</span></a></li>
\t\t\t\t";
        }
        // line 28
        echo "            </ul>
          </div>
        </div>
      </nav>
\t";
        
        $__internal_9ab209e0001262560eb35460e4f138b1188b705fc096ad7c2dcb4aa9de17fc3e->leave($__internal_9ab209e0001262560eb35460e4f138b1188b705fc096ad7c2dcb4aa9de17fc3e_prof);

    }

    // line 33
    public function block_content($context, array $blocks = array())
    {
        $__internal_59ed4b2db42be411fa5dec5d032b20c7888d06a59d1c717cabfba1780d31b7e8 = $this->env->getExtension("native_profiler");
        $__internal_59ed4b2db42be411fa5dec5d032b20c7888d06a59d1c717cabfba1780d31b7e8->enter($__internal_59ed4b2db42be411fa5dec5d032b20c7888d06a59d1c717cabfba1780d31b7e8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 34
        echo "\t";
        
        $__internal_59ed4b2db42be411fa5dec5d032b20c7888d06a59d1c717cabfba1780d31b7e8->leave($__internal_59ed4b2db42be411fa5dec5d032b20c7888d06a59d1c717cabfba1780d31b7e8_prof);

    }

    public function getTemplateName()
    {
        return "FrontendBundle::layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  127 => 34,  121 => 33,  110 => 28,  105 => 26,  100 => 25,  95 => 23,  90 => 22,  88 => 21,  82 => 18,  75 => 14,  64 => 5,  58 => 4,  50 => 35,  47 => 33,  45 => 4,  42 => 3,  36 => 2,  11 => 1,);
    }
}
/* {% extends '::base.html.twig' %}*/
/* {% block body %}*/
/* <div class="container">*/
/* 	{% block header %}*/
/*       <nav class="navbar navbar-default">*/
/*         <div class="container-fluid">*/
/*           <div class="navbar-header">*/
/*             <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">*/
/*               <span class="sr-only">Toggle navigation</span>*/
/*               <span class="icon-bar"></span>*/
/*               <span class="icon-bar"></span>*/
/*               <span class="icon-bar"></span>*/
/*             </button>*/
/* 				<a class="navbar-brand" href="{{path('_home')}}">LeadGrab </a>*/
/*           </div>*/
/*           <div id="navbar" class="navbar-collapse collapse">*/
/*             <ul class="nav navbar-nav">*/
/*               <li class="active"><a href="{{path('_home')}}">Home</a></li>*/
/*             </ul>*/
/*             <ul class="nav navbar-nav navbar-right">*/
/* 				{% if is_granted("IS_AUTHENTICATED_REMEMBERED") %}*/
/* 					<li><a href="{{ path('user_profile_show') }}">Profile <span class="sr-only">(current)</span></a></li>*/
/* 					<li><a href="{{ path('user_logout') }}">Logout <span class="sr-only">(current)</span></a></li>*/
/* 				{% else %}*/
/* 					<li><a href="{{ path('user_login') }}">Login <span class="sr-only">(current)</span></a></li>*/
/* 					<li><a href="{{ path('user_register') }}">Register <span class="sr-only">(current)</span></a></li>*/
/* 				{% endif %}*/
/*             </ul>*/
/*           </div>*/
/*         </div>*/
/*       </nav>*/
/* 	{% endblock %}*/
/* 	{% block content %}*/
/* 	{% endblock %}*/
/* </div>*/
/* {% endblock %}*/
/* */
