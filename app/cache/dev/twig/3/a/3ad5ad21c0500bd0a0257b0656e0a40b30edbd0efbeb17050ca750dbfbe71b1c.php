<?php

/* TwigBundle:Exception:exception_full.html.twig */
class __TwigTemplate_a8de69fcfe1f0d20053a6c73e37bd1b471f73561cd7a0b7b0ff4cdd6f474ef1b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("TwigBundle::layout.html.twig", "TwigBundle:Exception:exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "TwigBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_82c1ba8c948f7d5e86d69369eb10f088b2d3a843ce18dcf6c6c46e937e6cf84e = $this->env->getExtension("native_profiler");
        $__internal_82c1ba8c948f7d5e86d69369eb10f088b2d3a843ce18dcf6c6c46e937e6cf84e->enter($__internal_82c1ba8c948f7d5e86d69369eb10f088b2d3a843ce18dcf6c6c46e937e6cf84e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_82c1ba8c948f7d5e86d69369eb10f088b2d3a843ce18dcf6c6c46e937e6cf84e->leave($__internal_82c1ba8c948f7d5e86d69369eb10f088b2d3a843ce18dcf6c6c46e937e6cf84e_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_626dd142c532803a8ce920d1f79a1aa8bba4c7b899eeb18c05b9b401390fda12 = $this->env->getExtension("native_profiler");
        $__internal_626dd142c532803a8ce920d1f79a1aa8bba4c7b899eeb18c05b9b401390fda12->enter($__internal_626dd142c532803a8ce920d1f79a1aa8bba4c7b899eeb18c05b9b401390fda12_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('request')->generateAbsoluteUrl($this->env->getExtension('asset')->getAssetUrl("bundles/framework/css/exception.css")), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
";
        
        $__internal_626dd142c532803a8ce920d1f79a1aa8bba4c7b899eeb18c05b9b401390fda12->leave($__internal_626dd142c532803a8ce920d1f79a1aa8bba4c7b899eeb18c05b9b401390fda12_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_979db10203fda18e39331c97f7dc49d7331e518425c47a7e84d3ff9c3290b5b5 = $this->env->getExtension("native_profiler");
        $__internal_979db10203fda18e39331c97f7dc49d7331e518425c47a7e84d3ff9c3290b5b5->enter($__internal_979db10203fda18e39331c97f7dc49d7331e518425c47a7e84d3ff9c3290b5b5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 8
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_979db10203fda18e39331c97f7dc49d7331e518425c47a7e84d3ff9c3290b5b5->leave($__internal_979db10203fda18e39331c97f7dc49d7331e518425c47a7e84d3ff9c3290b5b5_prof);

    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        $__internal_cae82072c04c4481bdf820a21f7198ca3baaef0512ec3564e334e4a0f272ed38 = $this->env->getExtension("native_profiler");
        $__internal_cae82072c04c4481bdf820a21f7198ca3baaef0512ec3564e334e4a0f272ed38->enter($__internal_cae82072c04c4481bdf820a21f7198ca3baaef0512ec3564e334e4a0f272ed38_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 12
        echo "    ";
        $this->loadTemplate("TwigBundle:Exception:exception.html.twig", "TwigBundle:Exception:exception_full.html.twig", 12)->display($context);
        
        $__internal_cae82072c04c4481bdf820a21f7198ca3baaef0512ec3564e334e4a0f272ed38->leave($__internal_cae82072c04c4481bdf820a21f7198ca3baaef0512ec3564e334e4a0f272ed38_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 12,  72 => 11,  58 => 8,  52 => 7,  42 => 4,  36 => 3,  11 => 1,);
    }
}
/* {% extends 'TwigBundle::layout.html.twig' %}*/
/* */
/* {% block head %}*/
/*     <link href="{{ absolute_url(asset('bundles/framework/css/exception.css')) }}" rel="stylesheet" type="text/css" media="all" />*/
/* {% endblock %}*/
/* */
/* {% block title %}*/
/*     {{ exception.message }} ({{ status_code }} {{ status_text }})*/
/* {% endblock %}*/
/* */
/* {% block body %}*/
/*     {% include 'TwigBundle:Exception:exception.html.twig' %}*/
/* {% endblock %}*/
/* */
