<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appDevUrlMatcher.
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appDevUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($pathinfo);
        $context = $this->context;
        $request = $this->request;

        if (0 === strpos($pathinfo, '/_')) {
            // _wdt
            if (0 === strpos($pathinfo, '/_wdt') && preg_match('#^/_wdt/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_wdt')), array (  '_controller' => 'web_profiler.controller.profiler:toolbarAction',));
            }

            if (0 === strpos($pathinfo, '/_profiler')) {
                // _profiler_home
                if (rtrim($pathinfo, '/') === '/_profiler') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', '_profiler_home');
                    }

                    return array (  '_controller' => 'web_profiler.controller.profiler:homeAction',  '_route' => '_profiler_home',);
                }

                if (0 === strpos($pathinfo, '/_profiler/search')) {
                    // _profiler_search
                    if ($pathinfo === '/_profiler/search') {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchAction',  '_route' => '_profiler_search',);
                    }

                    // _profiler_search_bar
                    if ($pathinfo === '/_profiler/search_bar') {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchBarAction',  '_route' => '_profiler_search_bar',);
                    }

                }

                // _profiler_purge
                if ($pathinfo === '/_profiler/purge') {
                    return array (  '_controller' => 'web_profiler.controller.profiler:purgeAction',  '_route' => '_profiler_purge',);
                }

                // _profiler_info
                if (0 === strpos($pathinfo, '/_profiler/info') && preg_match('#^/_profiler/info/(?P<about>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_info')), array (  '_controller' => 'web_profiler.controller.profiler:infoAction',));
                }

                // _profiler_phpinfo
                if ($pathinfo === '/_profiler/phpinfo') {
                    return array (  '_controller' => 'web_profiler.controller.profiler:phpinfoAction',  '_route' => '_profiler_phpinfo',);
                }

                // _profiler_search_results
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/search/results$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_search_results')), array (  '_controller' => 'web_profiler.controller.profiler:searchResultsAction',));
                }

                // _profiler
                if (preg_match('#^/_profiler/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler')), array (  '_controller' => 'web_profiler.controller.profiler:panelAction',));
                }

                // _profiler_router
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/router$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_router')), array (  '_controller' => 'web_profiler.controller.router:panelAction',));
                }

                // _profiler_exception
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception')), array (  '_controller' => 'web_profiler.controller.exception:showAction',));
                }

                // _profiler_exception_css
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception\\.css$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception_css')), array (  '_controller' => 'web_profiler.controller.exception:cssAction',));
                }

            }

            if (0 === strpos($pathinfo, '/_configurator')) {
                // _configurator_home
                if (rtrim($pathinfo, '/') === '/_configurator') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', '_configurator_home');
                    }

                    return array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::checkAction',  '_route' => '_configurator_home',);
                }

                // _configurator_step
                if (0 === strpos($pathinfo, '/_configurator/step') && preg_match('#^/_configurator/step/(?P<index>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_configurator_step')), array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::stepAction',));
                }

                // _configurator_final
                if ($pathinfo === '/_configurator/final') {
                    return array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::finalAction',  '_route' => '_configurator_final',);
                }

            }

            // _twig_error_test
            if (0 === strpos($pathinfo, '/_error') && preg_match('#^/_error/(?P<code>\\d+)(?:\\.(?P<_format>[^/]++))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_twig_error_test')), array (  '_controller' => 'twig.controller.preview_error:previewErrorPageAction',  '_format' => 'html',));
            }

        }

        if (0 === strpos($pathinfo, '/admin')) {
            // _admin
            if (rtrim($pathinfo, '/') === '/admin') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', '_admin');
                }

                return array (  '_controller' => 'AdminBundle\\Controller\\AdminController::indexAction',  '_route' => '_admin',);
            }

            if (0 === strpos($pathinfo, '/admin/customers')) {
                // admin_customers
                if (rtrim($pathinfo, '/') === '/admin/customers') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_admin_customers;
                    }

                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'admin_customers');
                    }

                    return array (  '_controller' => 'AdminBundle\\Controller\\AdminCustomersController::indexAction',  '_route' => 'admin_customers',);
                }
                not_admin_customers:

                // admin_customers_create
                if ($pathinfo === '/admin/customers/') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_admin_customers_create;
                    }

                    return array (  '_controller' => 'AdminBundle\\Controller\\AdminCustomersController::createAction',  '_route' => 'admin_customers_create',);
                }
                not_admin_customers_create:

                // admin_customers_new
                if ($pathinfo === '/admin/customers/new') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_admin_customers_new;
                    }

                    return array (  '_controller' => 'AdminBundle\\Controller\\AdminCustomersController::newAction',  '_route' => 'admin_customers_new',);
                }
                not_admin_customers_new:

                // admin_customers_show
                if (preg_match('#^/admin/customers/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_admin_customers_show;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_customers_show')), array (  '_controller' => 'AdminBundle\\Controller\\AdminCustomersController::showAction',));
                }
                not_admin_customers_show:

                // admin_customers_edit
                if (preg_match('#^/admin/customers/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_admin_customers_edit;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_customers_edit')), array (  '_controller' => 'AdminBundle\\Controller\\AdminCustomersController::editAction',));
                }
                not_admin_customers_edit:

                // admin_customers_update
                if (preg_match('#^/admin/customers/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if ($this->context->getMethod() != 'PUT') {
                        $allow[] = 'PUT';
                        goto not_admin_customers_update;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_customers_update')), array (  '_controller' => 'AdminBundle\\Controller\\AdminCustomersController::updateAction',));
                }
                not_admin_customers_update:

                // admin_customers_delete
                if (preg_match('#^/admin/customers/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if ($this->context->getMethod() != 'DELETE') {
                        $allow[] = 'DELETE';
                        goto not_admin_customers_delete;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_customers_delete')), array (  '_controller' => 'AdminBundle\\Controller\\AdminCustomersController::deleteAction',));
                }
                not_admin_customers_delete:

            }

            if (0 === strpos($pathinfo, '/admin/roles')) {
                // admin_roles
                if (rtrim($pathinfo, '/') === '/admin/roles') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_admin_roles;
                    }

                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'admin_roles');
                    }

                    return array (  '_controller' => 'AdminBundle\\Controller\\AdminRolesController::indexAction',  '_route' => 'admin_roles',);
                }
                not_admin_roles:

                // admin_roles_create
                if ($pathinfo === '/admin/roles/') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_admin_roles_create;
                    }

                    return array (  '_controller' => 'AdminBundle\\Controller\\AdminRolesController::createAction',  '_route' => 'admin_roles_create',);
                }
                not_admin_roles_create:

                // admin_roles_new
                if ($pathinfo === '/admin/roles/new') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_admin_roles_new;
                    }

                    return array (  '_controller' => 'AdminBundle\\Controller\\AdminRolesController::newAction',  '_route' => 'admin_roles_new',);
                }
                not_admin_roles_new:

                // admin_roles_show
                if (preg_match('#^/admin/roles/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_admin_roles_show;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_roles_show')), array (  '_controller' => 'AdminBundle\\Controller\\AdminRolesController::showAction',));
                }
                not_admin_roles_show:

                // admin_roles_edit
                if (preg_match('#^/admin/roles/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_admin_roles_edit;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_roles_edit')), array (  '_controller' => 'AdminBundle\\Controller\\AdminRolesController::editAction',));
                }
                not_admin_roles_edit:

                // admin_roles_update
                if (preg_match('#^/admin/roles/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if ($this->context->getMethod() != 'PUT') {
                        $allow[] = 'PUT';
                        goto not_admin_roles_update;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_roles_update')), array (  '_controller' => 'AdminBundle\\Controller\\AdminRolesController::updateAction',));
                }
                not_admin_roles_update:

                // admin_roles_delete
                if (preg_match('#^/admin/roles/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if ($this->context->getMethod() != 'DELETE') {
                        $allow[] = 'DELETE';
                        goto not_admin_roles_delete;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_roles_delete')), array (  '_controller' => 'AdminBundle\\Controller\\AdminRolesController::deleteAction',));
                }
                not_admin_roles_delete:

            }

            if (0 === strpos($pathinfo, '/admin/login')) {
                // admin_login
                if ($pathinfo === '/admin/login') {
                    return array (  '_controller' => 'AdminBundle\\Controller\\AdminUsersController::loginAction',  '_route' => 'admin_login',);
                }

                // admin_login_check
                if ($pathinfo === '/admin/login_check') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_admin_login_check;
                    }

                    return array (  '_controller' => 'AdminBundle\\Controller\\AdminUsersController::loginCheckAction',  '_route' => 'admin_login_check',);
                }
                not_admin_login_check:

            }

            // admin_user_profile_show
            if ($pathinfo === '/admin/profile') {
                return array (  '_controller' => 'AdminBundle\\Controller\\AdminUsersController::adminProfileAction',  '_route' => 'admin_user_profile_show',);
            }

            // admin_logout
            if ($pathinfo === '/admin/logout') {
                return array (  '_controller' => 'AdminBundle\\Controller\\AdminUsersController::logoutAction',  '_route' => 'admin_logout',);
            }

            // admin_user_list
            if ($pathinfo === '/admin/user-list') {
                return array (  '_controller' => 'AdminBundle\\Controller\\AdminUsersController::userListAction',  '_route' => 'admin_user_list',);
            }

            // admin_profile_show
            if (0 === strpos($pathinfo, '/admin/profile') && preg_match('#^/admin/profile/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_profile_show')), array (  '_controller' => 'AdminBundle\\Controller\\AdminUsersController::profileAction',));
            }

            // admin_profile_edit
            if (preg_match('#^/admin/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_profile_edit')), array (  '_controller' => 'AdminBundle\\Controller\\AdminUsersController::editAction',));
            }

            // admin_profile_update
            if (preg_match('#^/admin/(?P<id>[^/]++)/update$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_profile_update')), array (  '_controller' => 'AdminBundle\\Controller\\AdminUsersController::updateAction',));
            }

            // admin_user_add
            if ($pathinfo === '/admin/add') {
                return array (  '_controller' => 'AdminBundle\\Controller\\AdminUsersController::addUserAction',  '_route' => 'admin_user_add',);
            }

            // admin_profile_delete
            if (preg_match('#^/admin/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_profile_delete')), array (  '_controller' => 'AdminBundle\\Controller\\AdminUsersController::deleteAction',));
            }

        }

        if (0 === strpos($pathinfo, '/log')) {
            if (0 === strpos($pathinfo, '/login')) {
                // user_login
                if ($pathinfo === '/login') {
                    return array (  '_controller' => 'AppBundle\\Controller\\UserController::loginAction',  '_route' => 'user_login',);
                }

                // user_login_check
                if ($pathinfo === '/login_check') {
                    return array (  '_controller' => 'AppBundle\\Controller\\UserController::loginCheckAction',  '_route' => 'user_login_check',);
                }

            }

            // user_logout
            if ($pathinfo === '/logout') {
                return array (  '_controller' => 'AppBundle\\Controller\\UserController::logoutAction',  '_route' => 'user_logout',);
            }

        }

        // user_mail
        if ($pathinfo === '/mail') {
            return array (  '_controller' => 'AppBundle\\Controller\\UserController::mailUserAction',  '_route' => 'user_mail',);
        }

        // user_register
        if ($pathinfo === '/register') {
            return array (  '_controller' => 'AppBundle\\Controller\\UserController::registerAction',  '_route' => 'user_register',);
        }

        // user_authenticate_process
        if (0 === strpos($pathinfo, '/authenticate') && preg_match('#^/authenticate/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'user_authenticate_process')), array (  '_controller' => 'AppBundle\\Controller\\UserController::authenticateProcessAction',));
        }

        // user_complete_profile
        if ($pathinfo === '/complete') {
            return array (  '_controller' => 'AppBundle\\Controller\\UserController::completeprofileAction',  '_route' => 'user_complete_profile',);
        }

        // user_profile_show
        if ($pathinfo === '/profile') {
            return array (  '_controller' => 'AppBundle\\Controller\\UserController::profileAction',  '_route' => 'user_profile_show',);
        }

        // user_profile_edit
        if (preg_match('#^/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'user_profile_edit')), array (  '_controller' => 'AppBundle\\Controller\\UserController::editAction',));
        }

        // user_profile_update
        if (preg_match('#^/(?P<id>[^/]++)/update$#s', $pathinfo, $matches)) {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_user_profile_update;
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'user_profile_update')), array (  '_controller' => 'AppBundle\\Controller\\UserController::updateAction',));
        }
        not_user_profile_update:

        // user_forgot_password
        if ($pathinfo === '/user/forgot-password') {
            return array (  '_controller' => 'AppBundle\\Controller\\UserController::forgotPasswordAction',  '_route' => 'user_forgot_password',);
        }

        // user_password_reset_process
        if (preg_match('#^/(?P<token>[^/]++)/passwordResetProcess$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'user_password_reset_process')), array (  '_controller' => 'AppBundle\\Controller\\UserController::passwordResetProcessAction',));
        }

        if (0 === strpos($pathinfo, '/customers')) {
            // customers
            if (rtrim($pathinfo, '/') === '/customers') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_customers;
                }

                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'customers');
                }

                return array (  '_controller' => 'FrontendBundle\\Controller\\CustomersController::indexAction',  '_route' => 'customers',);
            }
            not_customers:

            // customers_show
            if (preg_match('#^/customers/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_customers_show;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'customers_show')), array (  '_controller' => 'FrontendBundle\\Controller\\CustomersController::showAction',));
            }
            not_customers_show:

        }

        // _home
        if (rtrim($pathinfo, '/') === '') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', '_home');
            }

            return array (  '_controller' => 'FrontendBundle\\Controller\\FrontendController::indexAction',  '_route' => '_home',);
        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
