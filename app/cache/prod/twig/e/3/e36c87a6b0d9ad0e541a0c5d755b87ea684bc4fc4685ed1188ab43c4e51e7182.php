<?php

/* AdminBundle:AdminUsers:login.html.twig */
class __TwigTemplate_f16e9a6e3027d64d70ca27b5bedb0604ebf5820375d0af1e99c883756ec0da34 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "AdminBundle:AdminUsers:login.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_body($context, array $blocks = array())
    {
        // line 3
        echo "<div class=\"container\">
\t";
        // line 4
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "notice"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 5
            echo "\t<div class=\"flash-notice\">
\t\t<h2 class=\"btn btn-xs btn-danger\">";
            // line 6
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</h2>
\t</div>
\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 9
        echo "\t<form action=\"";
        echo $this->env->getExtension('routing')->getPath("admin_login_check");
        echo "\" method=\"post\" class=\"form-signin\">
\t\t<h2 class=\"form-signin-heading\">Admin Login !</h2>
\t\t<label class=\"sr-only\" for=\"username\">Username*</label>
\t\t\t<input type=\"text\" id=\"inputUsername\" class=\"form-control\" name=\"_username\" placeholder=\"Username\" required=\"\" autofocus=\"\">
\t\t<label class=\"sr-only\" for=\"password\">Password*</label>
\t\t\t<input type=\"password\" id=\"inputPassword\" class=\"form-control\" name=\"_password\" placeholder=\"Password\" required=\"\">
\t\t<button class=\"btn btn-lg btn-primary btn-block\" type=\"submit\">Sign in</button>\t</form>
\t</form>
</div>
";
    }

    public function getTemplateName()
    {
        return "AdminBundle:AdminUsers:login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  50 => 9,  41 => 6,  38 => 5,  34 => 4,  31 => 3,  28 => 2,  11 => 1,);
    }
}
/* {% extends '::base.html.twig' %}*/
/* {% block body %}*/
/* <div class="container">*/
/* 	{% for flashMessage in app.session.flashbag.get('notice') %}*/
/* 	<div class="flash-notice">*/
/* 		<h2 class="btn btn-xs btn-danger">{{ flashMessage }}</h2>*/
/* 	</div>*/
/* 	{% endfor %}*/
/* 	<form action="{{ path('admin_login_check') }}" method="post" class="form-signin">*/
/* 		<h2 class="form-signin-heading">Admin Login !</h2>*/
/* 		<label class="sr-only" for="username">Username*</label>*/
/* 			<input type="text" id="inputUsername" class="form-control" name="_username" placeholder="Username" required="" autofocus="">*/
/* 		<label class="sr-only" for="password">Password*</label>*/
/* 			<input type="password" id="inputPassword" class="form-control" name="_password" placeholder="Password" required="">*/
/* 		<button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>	</form>*/
/* 	</form>*/
/* </div>*/
/* {% endblock %}*/
/* */
