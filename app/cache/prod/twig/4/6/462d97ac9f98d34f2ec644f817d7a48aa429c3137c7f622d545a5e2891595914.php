<?php

/* ::base.html.twig */
class __TwigTemplate_dc12eeebe16666ac77a1b3daff0d97a05caab3ae2236c6af83c5c419968339f2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'javascriptss' => array($this, 'block_javascriptss'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
\t\t";
        // line 4
        $this->displayBlock('head', $context, $blocks);
        // line 6
        echo "\t\t<meta charset=\"UTF-8\" />
\t\t<title>";
        // line 7
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
\t\t<link rel=\"icon\" href=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/images/favicon.ico"), "html", null, true);
        echo "\">
\t\t";
        // line 9
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 17
        echo "\t\t";
        $this->displayBlock('javascriptss', $context, $blocks);
        // line 20
        echo "    </head>
    <body>
        ";
        // line 22
        $this->displayBlock('body', $context, $blocks);
        // line 24
        echo "        ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 29
        echo "    </body>
</html>
";
    }

    // line 4
    public function block_head($context, array $blocks = array())
    {
        // line 5
        echo "\t\t";
    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        echo "Welcome LeadGrab !";
    }

    // line 9
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 10
        echo "\t\t\t<!-- Bootstrap core CSS -->
\t\t\t<link href=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/css/bootstrap.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
\t\t\t<!-- Custom styles for this template -->
\t\t\t<link href=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/css/navbar.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
\t\t\t<link href=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/css/dashboard.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
\t\t\t<link href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/css/signin.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
\t\t";
    }

    // line 17
    public function block_javascriptss($context, array $blocks = array())
    {
        // line 18
        echo "\t\t\t<script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/js/ie-emulation-modes-warning.js"), "html", null, true);
        echo "\"></script>
\t\t";
    }

    // line 22
    public function block_body($context, array $blocks = array())
    {
        // line 23
        echo "        ";
    }

    // line 24
    public function block_javascripts($context, array $blocks = array())
    {
        // line 25
        echo "\t\t\t<script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/js/jquery.min.js"), "html", null, true);
        echo "\"></script>
\t\t\t<script src=\"";
        // line 26
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
\t\t\t<script src=\"";
        // line 27
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/js/ie10-viewport-bug-workaround.js"), "html", null, true);
        echo "\"></script>
\t\t";
    }

    public function getTemplateName()
    {
        return "::base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  130 => 27,  126 => 26,  121 => 25,  118 => 24,  114 => 23,  111 => 22,  104 => 18,  101 => 17,  95 => 15,  91 => 14,  87 => 13,  82 => 11,  79 => 10,  76 => 9,  70 => 7,  66 => 5,  63 => 4,  57 => 29,  54 => 24,  52 => 22,  48 => 20,  45 => 17,  43 => 9,  39 => 8,  35 => 7,  32 => 6,  30 => 4,  25 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html>*/
/*     <head>*/
/* 		{% block head %}*/
/* 		{% endblock %}*/
/* 		<meta charset="UTF-8" />*/
/* 		<title>{% block title %}Welcome LeadGrab !{% endblock %}</title>*/
/* 		<link rel="icon" href="{{ asset('bundles/frontend/images/favicon.ico') }}">*/
/* 		{% block stylesheets %}*/
/* 			<!-- Bootstrap core CSS -->*/
/* 			<link href="{{ asset('bundles/frontend/css/bootstrap.min.css') }}" rel="stylesheet">*/
/* 			<!-- Custom styles for this template -->*/
/* 			<link href="{{ asset('bundles/frontend/css/navbar.css') }}" rel="stylesheet">*/
/* 			<link href="{{ asset('bundles/frontend/css/dashboard.css') }}" rel="stylesheet">*/
/* 			<link href="{{ asset('bundles/frontend/css/signin.css') }}" rel="stylesheet">*/
/* 		{% endblock %}*/
/* 		{% block javascriptss %}*/
/* 			<script src="{{ asset('bundles/frontend/js/ie-emulation-modes-warning.js') }}"></script>*/
/* 		{% endblock %}*/
/*     </head>*/
/*     <body>*/
/*         {% block body %}*/
/*         {% endblock %}*/
/*         {% block javascripts %}*/
/* 			<script src="{{ asset('bundles/frontend/js/jquery.min.js') }}"></script>*/
/* 			<script src="{{ asset('bundles/frontend/js/bootstrap.min.js') }}"></script>*/
/* 			<script src="{{ asset('bundles/frontend/js/ie10-viewport-bug-workaround.js') }}"></script>*/
/* 		{% endblock %}*/
/*     </body>*/
/* </html>*/
/* */
