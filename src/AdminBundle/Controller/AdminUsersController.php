<?php

namespace AdminBundle\Controller;

use AppBundle\Entity\User;
use AdminBundle\Form\UserAddType;
use AdminBundle\Form\UserUpdateType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

/**
 * AdminUsersController
 * @Route("/admin")
 * @author Dheerendra <dhee1789@gmail.com>
 */
class AdminUsersController extends Controller
{
	/**
	 * @Route("/login", name="admin_login")
	 * @Template()
	 */
	public function loginAction()
	{
		//Change This Access After First Project Installation 
		if ($this->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')) {
			// Logged in user's context
			$securityContext = $this->get('security.context');
			$user = $securityContext->getToken()->getUser();
			return $this->redirect($this->generateUrl('_admin'));
		} else {
			$this->get('session')->getFlashBag()->add('notice', 'Bad Credentials');
		}
		$request = $this->getRequest();
		$session = $request->getSession();
		$error = $session->get(SecurityContext::AUTHENTICATION_ERROR);
		$session->remove(SecurityContext::AUTHENTICATION_ERROR);
		return array(
			'last_username' => $session->get(SecurityContext::LAST_USERNAME),
			'error' => $error,
		);
	}

	/**
	 * @Route("/login_check", name="admin_login_check")
	 * @Method("POST")
	 * @Template()
	 */
	public function loginCheckAction(Request $request)
	{
		if ($_POST) {
			$em = $this->getDoctrine()->getEntityManager();
			$user_name = $this->getRequest()->request->get('_username');
			$pass_word = $this->getRequest()->request->get('_password');
			$user = $em->getRepository('AppBundle:User')->findOneBy(array('username' => $user_name, 'plainPassword' => $pass_word));
			if (isset($user)) {
				$user_roles = $user->getRoles();
				//Change This Role Access Before Admin Run
				if (in_array('ROLE_SUPER_ADMIN', $user_roles)) {
					$this->authenticateUser($user, $user->getRoles());
					return $this->redirect($this->generateUrl('_admin'));
				}elseif(in_array('ROLE_ADMIN', $user_roles)){
					$this->authenticateUser($user, $user->getRoles());
					return $this->redirect($this->generateUrl('_admin'));
				}else {
					$this->get('session')->getFlashBag()->add('error', 'Access Denied');
					return $this->redirect($this->generateUrl('admin_login'));
				}
			} else {
				$this->get('session')->getFlashBag()->add('error', 'Please enter valid username and password');
				return $this->redirect($this->generateUrl('admin_login'));
			}
		}
	}

	/**
	 *  User Login 
	 */
	private function authenticateUser(UserInterface $user, $role)
	{
		$provideKey = 'user_secured_area'; // MAKE A KEY in YAML and get it here
		$token = new UsernamePasswordToken($user, null, $provideKey, $role);
		$this->get('security.context')->setToken($token);
	}


    /**
     * Finds and displays a User Entity.
     *
     * @Route("/profile", name="admin_user_profile_show")
     * @Template()
     */
    public function adminProfileAction(Request $request) {

        $session = new Session();
        $s = $session->get('current_locale');
        $this->getRequest()->setLocale($s);
        $securityContext = $this->get('security.context');

        if (false === $securityContext->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $this->redirect($this->generateUrl('admin_login'));
        }

        $userContext = $securityContext->getToken()->getUser();
        $em = $this->getDoctrine()->getEntityManager();
        $user = $em->getRepository('AppBundle:User')->findOneBy(array('id' => $userContext->getId()));

        if (!$user) {
            throw $this->createNotFoundException('No user found.');
        }

        return array(
            'user' => $user,
        );
    }


	/**
	 * @Route("/logout", name="admin_logout")
	 * @Template()
	 */
	public function logoutAction()
	{
		session_destroy();
		return $this->redirect($this->generateUrl('admin_login'));
	}

	/**
	 * @Route("/user-list", name="admin_user_list" )
	 * @Template()
	 */
	public function userListAction(Request $request)
	{
		$em = $this->getDoctrine()->getEntityManager();
		$form = $this->createFormBuilder()
				->add('name', 'text', array('required' => false, 'mapped' => false, 'attr' => array('data-errormessage-value-missing' => 'User Name is required')))
				->getForm();
		$userLists = $em->getRepository('AppBundle:User')->findAll();
		if ($request->getMethod()) {
			$form->bind($request);
			if ($form->isValid()) {
				$productValue = $form['name']->getData();
				$userLists = $em->getRepository('AppBundle:User')->findBy(array('username' => $productValue));
			}
		}
		return array(
			'userLists' => $userLists,
			'form' => $form->createView(),
		);
	}

	/**
	 * Finds and displays a User Entity.
	 *
	 * @Route("/profile/{id}", name="admin_profile_show")
	 * @Template()
	 */
	public function profileAction($id)
	{
		$em = $this->getDoctrine()->getEntityManager();
		$user = $em->getRepository('AppBundle:User')->findOneBy(array('id' => $id));
		if (!$user) {
			throw $this->createNotFoundException('No user found.');
		}
		return array(
			'user' => $user,
		);
	}

	/**
	 * Displays a form to edit an existing User entity.
	 *
	 * @Route("/{id}/edit", name="admin_profile_edit")
	 * @Template()
	 */
	public function editAction($id)
	{
		$em = $this->getDoctrine()->getManager();
		$securityContext = $this->get('security.context');
		$admin = $securityContext->getToken()->getUser();
		$user = $em->getRepository('AppBundle:User')->findOneBy(array('id' => $id));
		$roles = $admin->getRoles();
		if (!$user) {
			throw $this->createNotFoundException('Unable to find User entity.');
		}
		$editForm = $this->createForm(new UserUpdateType(), $user);
		$deleteForm = $this->createDeleteForm($id);
		return array(
			'roles' => $roles,
			'user' => $user,
			'edit_form' => $editForm->createView(),
			'delete_form' => $deleteForm->createView(),
		);
	}

	/**
	 * Edits an existing User entity.
	 *
	 * @Route("/{id}/update", name="admin_profile_update")
	 * @Template("AdminBundle:AdminUsers:edit.html.twig")
	 */
	public function updateAction(Request $request, $id)
	{
		$em = $this->getDoctrine()->getManager();
		$user = $em->getRepository('AppBundle:User')->findOneBy(array('id' => $id));
		if (!$user) {
			throw $this->createNotFoundException('Unable to find User entity.');
		}
		$deleteForm = $this->createDeleteForm($id);
		$editForm = $this->createForm(new UserUpdateType(), $user);
		if ('POST' === $request->getMethod()) {
			$editForm->handleRequest($request);

			if ($editForm->isValid()) {
				$em->persist($user);
				$em->flush();
				$this->addFlash(
					'notice',
					'Profile has been updated !'
				);
				return $this->redirect($this->generateUrl('admin_profile_edit', array('id' => $user->getId())));
			}
		}
		return array(
			'user' => $user,
			'edit_form' => $editForm->createView(),
			'delete_form' => $deleteForm->createView(),
		);
	}

    /**
     * @Route("/add", name = "admin_user_add")
     * @Template()
     */
    public function addUserAction(Request $request)
    {
        $user = new User();
        $form = $this->createForm(new UserAddType(), $user);
        $token = $this->generateToken();
        if ('POST' == $request->getMethod()) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                //echo "string";die;
                $encoder = $this->get('security.encoder_factory')->getEncoder($user);
                $user->setPassword($encoder->encodePassword($user->getPlainPassword(), $user->getSalt()));
                $user->setToken($token);
                $em = $this->getDoctrine()->getEntityManager();
                $em->persist($user);
                $em->flush();
				$this->addFlash(
					'success',
					'User Created !'
				);
                return $this->redirect($this->generateUrl('admin_user_list'));
            }
        }
        return array('form' => $form->createView());
    }

	private function encodePassword($user, $plainPassword)
	{
		$encoder = $this->container->get('security.encoder_factory')->getEncoder($user);
		return $encoder->encodePassword($plainPassword, $user->getSalt());
	}

	private function generateToken()
	{
		$key = '#}~*$/"$?&*(."*/[!%]/${"/}';
		$unique = uniqid();
		return $token = $unique . substr(hash('sha512', $unique . $key . microtime()), 0, 19);
	}

	/**
 	 * Deletes a User entity.
	 *
	 * @Route("/{id}/delete", name="admin_profile_delete")
	 */
    public function deleteAction(Request $request, $id)
	{
		//$form = $this->createDeleteForm($id);
		if ('GET' === $request->getMethod()) {
				$em = $this->getDoctrine()->getManager();
				$user = $em->getRepository('AppBundle:User')->find($id);
				if (!$user) {
					throw $this->createNotFoundException('Unable to find User entity.');
				}
				$em->remove($user);
				$em->flush();
				$this->addFlash(
					'notice',
					'User has been deleted !'
				);
		}

		return $this->redirect($this->generateUrl('admin_user_list'));
	}

	private function createDeleteForm($id)
	{
		return $this->createFormBuilder(array('id' => $id))
						->add('id', 'hidden')
						->getForm()
		;
	}

}
