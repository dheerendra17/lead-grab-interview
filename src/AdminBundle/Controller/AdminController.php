<?php

namespace AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * AdminController
 * @Route("/admin")
 * @author Dheerendra <dhee1789@gmail.com>
 */
class AdminController extends Controller
{
	/**
	 * @Route("/", name="_admin")
	 * @Template()
	 */
	public function indexAction()
	{
		$session = new Session();
		$s = $session->get('current_locale');
		$this->getRequest()->setLocale($s);

		//Change This Access After First Project Installation 
		if(TRUE === $this->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')){
			// Logged in user's context
			$securityContext = $this->get('security.context');
			$user = $securityContext->getToken()->getUser();
			return array('user'=>$user);
		}
		else {
			return $this->redirect($this->generateUrl('admin_login'));
		}

		return $this->redirect($this->generateUrl('admin_login'));

	}

}
