<?php
namespace AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\MinLength;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

/**
 * @author Dheerendra <dhee1789@gmail.com>
 */
class UserAddType extends AbstractType
{
	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$choice = array('1'=>'Active', '0'=>'Disable');
		$builder
            ->add('username', 'text', array('label'=> 'User Name*', 'required'=>false))
            ->add('email', 'email', array('label'=> 'Email*', 'required'=>false))
			->add('plainpassword', 'repeated', array(
					'type' => 'password',
					'invalid_message' => 'The password fields must match.',
					'options' => array('attr' => array('class' => 'password-field')),
					'required' => false,
					'first_options'  => array('label' => 'Password'),
					'second_options' => array('label' => 'Repeat Password'),
			))
			->add('isActive', 'choice', array(
				'choices'   => $choice,
				'required' => false
			))
            ->add('phone_number','text',array('required'=>false))
            ->add('city','text',array('required'=>false))
            ->add('country','text',array('required'=>false))
		;
	}

	/**
	 * @param OptionsResolverInterface $resolver
	 */
	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'AppBundle\Entity\User'
		));
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return 'adminbundle_user_add';
	}

}
