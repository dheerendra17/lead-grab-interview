<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use EWZ\Bundle\RecaptchaBundle\Validator\Constraints as Recaptcha;
use EWZ\Bundle\RecaptchaBundle\Validator\Constraints\True;

/**
 * @author Dheerendra <dhee1789@gmail.com>
 */
class RegisterUserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', 'text', array('label'=> 'User Name*', 'required'=>false))
            ->add('email', 'email', array('label'=> 'Email*', 'required'=>false))
			->add('plainpassword', 'repeated', array(
					'type' => 'password',
					'invalid_message' => 'The password fields must match.',
					'options' => array('attr' => array('class' => 'password-field')),
					'required' => false,
					'first_options'  => array('label' => 'Password'),
					'second_options' => array('label' => 'Repeat Password'),
			));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\User'
        ));
    }

    public function getName()
    {
        return 'user_register';
    }
}
