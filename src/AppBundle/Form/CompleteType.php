<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

/**
 * @author Dheerendra <dhee1789@gmail.com>
 */
class CompleteType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
				->add('first_name','text',array('required'=>false))
				->add('last_name','text',array('required'=>false))
				->add('phone_number','text',array('required'=>false))
				->add('gender', 'choice', array('choices' => array('1' => 'Male', '2' => 'Female')))
				->add('city')
				->add('country')
            ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\User'
        ));
    }

    public function getName()
    {
        return 'user_complete';
    }
}
