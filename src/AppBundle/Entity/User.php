<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\Common\Collection\ArrayCollection;
use EWZ\Bundle\RecaptchaBundle\Validator\Constraints as Recaptcha;
use Serializable;


/**
 * User
 * @author Dheerendra <dhee1789@gmail.com>
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Entity\UserRepository")
 * @UniqueEntity(fields="username", message="This username is not available")
 * @UniqueEntity(fields="email", message="This email address has already been registered. Try logging in.")
 * @ORM\HasLifecycleCallbacks
 */
class User implements AdvancedUserInterface, Serializable
{
    /**
     * @var integer $id
     *
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $username
     *
     * @ORM\Column(name="username", type="string", length=255, nullable = true)
     * @Assert\NotBlank(message = "You need to have a username")
     */
    private $username;

    /**
     * @var string $password
     *
     * @ORM\Column(name="password", type="string", length=255)
     */
    private $password;
    
    /**
     * @var string $salt
     *
     * @ORM\Column(name="salt", type="string", length=255)
     */
    private $salt;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Roles", inversedBy="user", cascade={"persist"})
     */
    private $roles;
    
    /**
     * @var string
     * 
     * @ORM\column(type="string", length=255, nullable=true)
     * @Assert\NotBlank(message="You need to enter an email address")
     * @Assert\Email(message="Your email seems invalid")
     */
    private $email;
    
    /**
     * @var string
     * 
     * @ORM\column(type="string", length=255, nullable=true)
     * @Assert\NotBlank(message="Password is required")
     * @Assert\Length(
     *      max = "6",
     *      maxMessage = "Your password cannot be longer than {{ limit }} characters length"
     * )
     */
    private $plainPassword;
    
     /**
     * @var string
     * 
     * @ORM\column(type="string", length=32, nullable=true)
     */
    private $token;
    
    /**
     * @var bool
     * 
     * @ORM\Column(type="boolean")
     */
    private $isActive = false;
   
    /**
     * @var string $first_name
     *
     * @ORM\Column(name="first_name", type="string", length=255, nullable=true)
     **/
    private $first_name;

    /**
     * @var string $last_name
     *
     * @ORM\Column(name="last_name", type="string", length=255, nullable=true)
     **/
    private $last_name;

    /**
     * @var string $gender
     *
     * @ORM\Column(name="gender", type="integer", nullable=true)
     **/
    private $gender;

    /**
     * @var string $phone_number
     *
     * @ORM\Column(name="phone_number", type="integer", length=15, nullable=true)
     * @Assert\Regex(
     *     pattern="/\d/",
     *     match=true,
     *     message="Only number"
     * )
     * @Assert\Length(
     *      min = "8",
     *      max = "11",
     *      minMessage = "Your phone number must be at least {{ limit }} characters length",
     *      maxMessage = "Your phone number cannot be longer than {{ limit }} characters length"
     * )
     **/
    private $phone_number;

    /**
     * @var string $address
     *
     * @ORM\Column(name="address", type="text", length=255, nullable=true)
     **/
    private $address;

    /**
     * @var string $zip_code
     *
     * @ORM\Column(name="zip_code", type="string", length=255, nullable=true)
     **/
    private $zip_code;

    /**
     *
     * @ORM\Column(name="city", type="text", length=255, nullable = true)
     */
    private $city;

    /**
     *
     * @ORM\Column(name="country", type="text", length=255, nullable = true)
     */
    private $country;

    /**
     * Constructor
     */
    public function __construct(array $roles = array())
    {
		$this->roles = $roles;
		$this->salt = base_convert(sha1(uniqid(mt_rand(),true)),16,36);
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set username
     *
     * @param string $username
     * @return User
     */
    public function setUsername($username)
    {
        $this->username = $username;
    
        return $this;
    }

    /**
     * Get username
     *
     * @return string 
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;
    
        return $this;
    }

    /**
     * Get password
     *
     * @return string 
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set salt
     *
     * @param string $salt
     * @return User
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;
    
        return $this;
    }

    /**
     * Get salt
     *
     * @return string 
     */
    public function getSalt()
    {
        return $this->salt;
    }
    	  
    public function equals(UserInterface $user)
    {
		return $this->getId() == $user->getId();
    }	
    
    public function eraseCredentials()
    {
		//$this->setPlainPassword(null);
	}

    /**
     * Set isActive
     *
     * @param boolean $isActive
     * @return User
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
    
        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean 
     */
    public function getIsActive()
    {
        return $this->isActive;
    }
    /**
     * Get isAccountNonExpired
     *
     * @return boolean 
     */
    public function isAccountNonExpired()
    {
		return true;
	}
	
	/**
     * Get isAccountNonLocked
     *
     * @return boolean 
     */
	public function isAccountNonLocked()
	{
		return true;
	}
	
	/**
     * Get isAccountNonExpired
     *
     * @return boolean 
     */
	public function isCredentialsNonExpired()
	{
		return true;
	}
	
	/**
     * Get isEnabled
     *
     * @return boolean 
     */
	
	public function isEnabled()
	{
		return $this->getIsActive();
	}
    
    public function serialize()
    {
		return serialize(array(
        	'username' => $this->getUsername()
        ));
	}
	
	public function unserialize($serialized)
	{
		$data = unserialize($serialized);
		$this->username = $data['username'];
	}
	
    /**
     * Set email
     *
     * @param string $email
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;
    
        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set plainPassword
     *
     * @param string $plainPassword
     * @return User
     */
    public function setPlainPassword($plainPassword)
    {
        $this->plainPassword = $plainPassword;
    
        return $this;
    }

    /**
     * Get plainPassword
     *
     * @return string 
     */
    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

	public function getRoles()
    {
        return $this->roles->toArray();
    }

    public function setRoles($roles)
    {
        $this->roles = $roles;
    }

    /**
     * Set token
     *
     * @param string $token
     * @return User
     */
    public function setToken($token)
    {
        $this->token = $token;
    
        return $this;
    }

    /**
     * Get token
     *
     * @return string 
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * Set first_name
     *
     * @param string $firstName
     * @return User
     */
    public function setFirstName($firstName)
    {
        $this->first_name = $firstName;
    
        return $this;
    }

    /**
     * Get first_name
     *
     * @return string 
     */
    public function getFirstName()
    {
        return $this->first_name;
    }

    /**
     * Set last_name
     *
     * @param string $lastName
     * @return User
     */
    public function setLastName($lastName)
    {
        $this->last_name = $lastName;
    
        return $this;
    }

    /**
     * Get last_name
     *
     * @return string 
     */
    public function getLastName()
    {
        return $this->last_name;
    }

    /**
     * Set created_date
     *
     * @param \DateTime $createdDate
     * @return User
     */
    public function setCreatedDate($createdDate)
    {
        $this->created_date = $createdDate;
    
        return $this;
    }

    /**
     * Get created_date
     *
     * @return \DateTime 
     */
    public function getCreatedDate()
    {
        return $this->created_date;
    }

    /**
     * Set gender
     *
     * @param integer $gender
     * @return User
     */
    public function setGender($gender)
    {
        $this->gender = $gender;
    
        return $this;
    }

    /**
     * Get gender
     *
     * @return integer 
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Set zip_code
     *
     * @param string $zipCode
     * @return User
     */
    public function setZipCode($zipCode)
    {
        $this->zip_code = $zipCode;
    
        return $this;
    }

    /**
     * Get zip_code
     *
     * @return string 
     */
    public function getZipCode()
    {
        return $this->zip_code;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return User
     */
    public function setAddress($address)
    {
        $this->address = $address;
    
        return $this;
    }

    /**
     * Get address
     *
     * @return string 
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set phone_number
     *
     * @param integer $phoneNumber
     * @return User
     */
    public function setPhoneNumber($phoneNumber)
    {
        $this->phone_number = $phoneNumber;
    
        return $this;
    }

    /**
     * Get phone_number
     *
     * @return integer 
     */
    public function getPhoneNumber()
    {
        return $this->phone_number;
    }


    /**
     * Set city
     *
     * @param string $city
     * @return User
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string 
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set country
     *
     * @param string $country
     * @return User
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return string 
     */
    public function getCountry()
    {
        return $this->country;
    }
}
