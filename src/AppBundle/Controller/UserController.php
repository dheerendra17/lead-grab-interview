<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Form\CompleteType;
use AppBundle\Form\RegisterUserType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Translation\Translator;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Translation\Loader\YamlFileLoader;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller as BaseController;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;


/**
 * User controller.
 * @author Dheerendra <dhee1789@gmail.com>
 * @Route("/")
 */
class UserController extends BaseController {

    /**
     * @Route("/login", name="user_login")
     * @Template()
     */
    public function loginAction()
    {
        if ($this->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')){
            // Logged in user's context
            $securityContext = $this->get('security.context');
            $user = $securityContext->getToken()->getUser();
            // Redirect authenticated users to profile by id reference
            return $this->redirect($this->generateUrl('_home'));
        } else {
            $this->get('session')->getFlashBag()->add('login_error', 'Bad Credentials');
        }
        $request = $this->getRequest();
        $session = $request->getSession();

        // Get the login error if there is one
        $error = $session->get(SecurityContext::AUTHENTICATION_ERROR);
        $session->remove(SecurityContext::AUTHENTICATION_ERROR);

        return array(
            // Last username entered by the user
            'last_username' => $session->get(SecurityContext::LAST_USERNAME),
            'error' => $error,
        );
    }

    /**
     * @Route("/login_check", name="user_login_check")
     * @Template()
     */
    public function loginCheckAction(){

	}

    /**
     * @Route("/logout", name="user_logout")
     * @Template()
     */
    public function logoutAction(){

    }

    /**
     * @Route("/mail", name="user_mail")
     * @Template()
     */
    public function mailUserAction()
    {
        $message = \Swift_Message::newInstance()
                ->setSubject('LeadGrab Interview')
                ->setFrom('dhee1789@gmail.com')
                ->setTo('dhee1789@gmail.com')
                ->setBody("Hello");
        if ($this->get('mailer')->send($message)) {
            $this->get('session')->getFlashBag()->add('success', 'You have been successfully sent mail');
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Your mail has been not sent');
        }
        return $this->redirect($this->generateUrl('user_login'));
    }

    /**
     * @Route("/register", name = "user_register")
     * @Template()
     */
    public function registerAction(Request $request)
    {
        $user = new User();
        $form = $this->createForm(new RegisterUserType(), $user);
        $token = $this->generateToken();
        if ('POST' == $request->getMethod()) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $encoder = $this->get('security.encoder_factory')->getEncoder($user);
                $user->setPassword($encoder->encodePassword($user->getPlainPassword(), $user->getSalt()));
                $user->setToken($token);
                $em = $this->getDoctrine()->getEntityManager();
                $em->persist($user);
                $em->flush();
				//Send a Email
                $message = \Swift_Message::newInstance()
                        ->setSubject('LeadGrab User Email Confirmation')
                        ->setFrom('dhee1789@gmail.com')
                        ->setTo($user->getEmail())
                        ->setBody($this->renderView(
                                'AppBundle:User/Email:registration.html.twig', array('token' => $token, 'user' => $user)), 'text/html'
                );
                $this->get('mailer')->send($message);
                $this->get('session')->getFlashBag()->add('success', 'You have been successfully registered, An email confirmation message has been sent to ' . $user->getEmail()
                );
                return $this->redirect($this->generateUrl('user_login'));
            }
        }
        return array('form' => $form->createView());
    }

    /**
     * @Route("/authenticate/{token}", name = "user_authenticate_process")
     * @Template()
     */
    public function authenticateProcessAction(Request $request, $token)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $user = $em->getRepository('AppBundle:User')->findOneBy(array('token' => $token));
        if (isset($user)) {
            if ($user->getIsActive() == false) {
                // Enabling The Account
                $user->setIsActive(true);
                $user->setToken(null);
                $em->persist($user);
                $em->flush();
                $this->authenticateUser($user);
                return array('user' => $user);
            }
        }
        $this->get('session')->getFlashBag()->add('error', 'This link is invalid or has been already used');
        return $this->redirect($this->generateUrl('user_login'));
    }

    /**
     * Complete Your Profile .
     *
     * @Route("/complete", name="user_complete_profile")
     * @Template()
     */
    public function completeprofileAction(Request $request)
    {
        // Logged in user's context
        $securityContext = $this->get('security.context');
        if (false === $securityContext->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $this->redirect($this->generateUrl('user_login'));
        }
        $userContext = $securityContext->getToken()->getUser();
        $em = $this->getDoctrine()->getEntityManager();
        $user = $em->getRepository('AppBundle:User')->findOneBy(array('id' => $userContext->getId()));
        $comp_form = $this->createForm(new CompleteType(), $user);
        if ('POST' === $request->getMethod()) {
            $comp_form->bind($request);
            if ($comp_form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($user);
                $em->flush();
                $this->get('session')->getFlashBag()->add('success', 'User Profile has been updated successfully');
                return $this->redirect($this->generateUrl('user_profile_show'));
            }
        }
        return array(
            'user' => $user,
            'comp_form' => $comp_form->createView()
        );
    }

    /**
     * Finds and displays a User Entity.
     *
     * @Route("/profile", name="user_profile_show")
     * @Template()
     */
    public function profileAction(Request $request)
    {
        $session = new Session();
        $s = $session->get('current_locale');
        $this->getRequest()->setLocale($s);
        $securityContext = $this->get('security.context');
        if (false === $securityContext->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $this->redirect($this->generateUrl('user_login'));
        }
        $userContext = $securityContext->getToken()->getUser();
        $em = $this->getDoctrine()->getEntityManager();
        $user = $em->getRepository('AppBundle:User')->findOneBy(array('id' => $userContext->getId()));
        if (!$user) {
            throw $this->createNotFoundException('No user found.');
        }

        return array(
            'user' => $user,
        );
    }

    /**
     * Displays a form to edit an existing User entity.
     *
     * @Route("/{id}/edit", name="user_profile_edit")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('AppBundle:User')->findOneBy(array('id' => $id));
        if (!$user) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }
        $editForm = $this->createForm(new CompleteType(), $user);
        $deleteForm = $this->createDeleteForm($id);
        return array(
            'user' => $user,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Edits an existing User entity.
     *
     * @Route("/{id}/update", name="user_profile_update")
     * @Method("POST")
     * @Template("AppBundle:User:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
		$em = $this->getDoctrine()->getManager();
		$user = $em->getRepository('AppBundle:User')->findOneBy(array('id' => $id));
        if (!$user) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }
        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new CompleteType(), $user);
        if ('POST' === $request->getMethod()) {
            $editForm->bind($request);
            if ($editForm->isValid()) {
                $em->persist($user);
                $em->flush();
                $this->get('session')->getFlashBag()->add('success', 'User Profile has been updated successfully');
                return $this->redirect($this->generateUrl('user_profile_show', array('id' => $user->getId())));
            }
        }
        return array(
            'user' => $user,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
                        ->add('id', 'hidden')
                        ->getForm()
        ;
    }

    /**
     * Renders Forgot Password Page
     * @Route("/user/forgot-password", name="user_forgot_password")
     */
    public function forgotPasswordAction(Request $request)
    {
        $token = $this->generateToken();
        $form = $this->createFormBuilder()
                        ->add('email', 'text', array('required' => false, 'constraints' =>
                            array(new NotBlank(array('message' => 'Email is required')),
                                new Email(array('message' => 'Email not valid')))
                        ))->getForm();
        if ('POST' === $request->getMethod()) {
            $form->bind($request);
            if ($form->isValid()) {
                $email = $form['email']->getData();
                $em = $this->getDoctrine()->getManager();
                $user = $em->getRepository('AppBundle:User')->findOneBy(array('email' => $email));
                if (isset($user)) {
                    $name = $user->getUsername();
                    $user->setIsActive(false);
                    $user->setToken($token);
                    $em->persist($user);
                    $em->flush();
					//Send a Email
                    $message = \Swift_Message::newInstance()
                            ->setSubject('Password Recovery Information')
                            ->setFrom('dhee1789@gmail.com') // GET FROM CONFIGURATION.
                            ->setTo($email)
                            ->setBody($this->renderView('AppBundle:User/Email:passwordRecoveryProcess.html.twig', array('user' => $user, 'token' => $token)), 'text/html');
                    if ($this->get('mailer')->send($message)) {
                        $this->get('session')->getFlashBag()->add('success', 'A link has been send in Email Address');
                        return $this->redirect($this->generateUrl('user_login'));
                    } else {
                        $this->get('session')->getFlashBag()->add('error', 'Email does not exist, Please try another email');
                        return $this->redirect($this->generateUrl('user_forgot_password'));
                    }
                } else {
                    $this->get('session')->getFlashBag()->add('error', 'Email does not exist, Please try another email');
                    return $this->redirect($this->generateUrl('user_forgot_password'));
                }
            }
        }
        return $this->render('AppBundle:User:forgotPassword.html.twig', array('form' => $form->createView()));
    }


    /**
     * Reset the Password .
     * @Route("/{token}/passwordResetProcess", name="user_password_reset_process")
     * @Template()
     */
    public function passwordResetProcessAction(Request $request, $token) {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('UserBundle:User')->findOneBy(array('token' => $token));

        if (isset($user)) {
            $token = $user->getToken();
            $status = $user->getIsActive();
            $name = $user->getUsername();

            if ($status == false) {
                $new_password_form = $this->createFormBuilder($user)
                        ->add('plainpassword', 'repeated', array(
                            'type' => 'password'))
                        ->getForm();

                if ('POST' === $request->getMethod()) {
                    $new_password_form->bind($request);

                    if ($new_password_form->isValid()) {
                        $user->setPassword($this->encodePassword($user, $user->getPlainPassword()));
                        $user->setPlainPassword($new_password_form['plainpassword']->getData());
                        $user->setIsActive(true);
                        $user->setToken(null);
                        $em->persist($user);
                        $em->flush();

						//Send Email
                        $message = \Swift_Message::newInstance()
                                ->setSubject('Password Reset Confirmation Mail')
                                ->setFrom('dhee1789@gmail.com')
                                ->setTo($user->getEmail())
                                ->setBody($this->renderView('UserBundle:User/Email:resetconfirmation.html.twig', array('token' => $token, 'user' => $user, 'name' => $name)), 'text/html');
                        $this->get('mailer')->send($message);
                        $this->get('session')->getFlashBag()->add('success', 'Your password has been changed successfully, Please check your mail');
                        return $this->redirect($this->generateUrl('user_login'));
                    }
                }
                return array(
                    'user' => $user,
                    'new_password_form' => $new_password_form->createView(),
                    'token' => $token);
            }
        }
        $this->get('session')->getFlashBag()->add('error', 'You have already use this link for password reset , Pls check your confirmation mail !, if did not receive the mail please re-enter your Email Id');
        return $this->redirect($this->generateUrl('user_forgot_password'));
    }

    private function encodePassword($user, $plainPassword)
    {
        $encoder = $this->container->get('security.encoder_factory')->getEncoder($user);
        return $encoder->encodePassword($plainPassword, $user->getSalt());
    }

    private function authenticateUser(UserInterface $user)
    {
        $provideKey = 'user_secured_area'; // MAKE A KEY in YAML and get it here
        $token = new UsernamePasswordToken($user, null, $provideKey, $user->getRoles());
        $this->get('security.context')->setToken($token);
    }

    private function generateToken()
    {
        $key = '#}~*$/"$?&*(."*/[!%]/${"/}';
        $unique = uniqid();
        return $token = $unique . substr(hash('sha512', $unique . $key . microtime()), 0, 19);
    }
}
